import axios from 'axios';

export const api = axios.create({
    baseURL: "https://secondhandkelompok4.herokuapp.com/api/"
})
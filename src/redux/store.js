import {configureStore} from '@reduxjs/toolkit'
import barangReducer from './barangSlice'
import  storage  from "redux-persist/lib/storage"
import { combineReducers } from '@reduxjs/toolkit'
import { persistReducer } from 'redux-persist'
import thunk from 'redux-thunk'
import authReducer from './authSlice'

const reducers = combineReducers({
    barangs: barangReducer,
    auth: authReducer
})

const persistConfig = {
    key: "root",
    storage
}

const persistedReducer = persistReducer(persistConfig, reducers)

const store = configureStore({
    reducer: persistedReducer,
    devTools: process.env.NODE_ENV !== 'production',
    middleware: [thunk]
})

export default store;
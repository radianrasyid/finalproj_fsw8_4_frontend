import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';

const BtnMdSecondary = (props) => {
  const { namaButton, linkHref, type, onClick } = props;

  return (
    <button className="btn btnSecondaryMd mt-4" type={type} href={linkHref} onClick={onClick}>
      {namaButton}
    </button>
  )
};

export default BtnMdSecondary;
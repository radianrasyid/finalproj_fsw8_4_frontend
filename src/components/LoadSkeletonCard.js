import React from "react";
import Skeleton from '@mui/material/Skeleton';
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";

const LoadingSkeletonCard = ({cards}) => {
    return Array (cards)
    .fill(0)
    .map((item, i) => (
        <Col key={i}>
        <Card style={{ minHeight: "280px", alignItems:"center"}} className="cardHome">
          <Skeleton 
            variant="rectangular"
            height={130} 
            width="90%" 
            animation="wave"
            className="pb-3"
            style={{ marginTop:"10px", borderRadius:"10px"}}/>
          <Skeleton 
            variant="text" 
            height={30} 
            width="85%" 
            animation="wave"
            style={{ marginTop:"8px"}} />
          <Skeleton 
            variant="text" 
            height={30} 
            width="85%" 
            animation="wave" />
          <Skeleton 
            variant="text" 
            height={30} 
            width="85%" 
            animation="wave" />
        </Card>
      </Col>
     )
    )
  };
  
  export default LoadingSkeletonCard; 
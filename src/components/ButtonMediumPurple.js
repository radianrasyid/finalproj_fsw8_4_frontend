import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';

const ButtonMediumPurple = (props) => {
  const { namaButton, linkHref, onClick, type } = props;

  return (
    <button className="btn btnPurpleMd mt-4" type={type} href={linkHref} onClick={onClick}>
      {namaButton}
    </button>
  )
};

export default ButtonMediumPurple;
import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ReactLoading from 'react-loading';

const LoadingPart = (props) => {
    return (
        <Container>
        <Row>
        <Col md={4}></Col>
        <Col md={4}>
        <div className="text-center loadingBar my-3">
            <ReactLoading type="bars" color="#7126b5"
            height={120} width={70} />
        </div>
        <h5 className='text-center mt-3' style={{color:"#a06ece"}}><b>Loading...</b></h5>
        </Col>
        <Col md={4}></Col>
        </Row>
        </Container>
    )
  };
  
  export default LoadingPart; 
import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import bttmHome from '../assets/img/bttmHome.svg';
import bttmDaftar from '../assets/img/bttmDaftar.svg';
import bttmJual from '../assets/img/bttmJual.svg';
import bttmNotif from '../assets/img/bttmNotif.svg';
import bttmAkun from '../assets/img/bttmAkun.svg';
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Col from 'react-bootstrap/Col';

const NavbarBottomAkun = (props) => {
    return (
        <Navbar className="fixed-bottom shadow-lg">
        <Nav>
        <div className='kontenNavbarBottom'>
          <Col sm={3}>
            <Nav.Link href="/" className='iconBttm'>
            <div className='iconBttm'> <img src={bttmHome} alt="" className='iconBttm'/> </div>
              <p className="barBottom">Home</p>
            </Nav.Link>
          </Col>

          <Col sm={3}>
            <Nav.Link href="/NotifikasiMobile" className='iconBttm'>
              <div style={{paddingLeft:"30%"}}> <img src={bttmNotif} alt=""/> </div>
              <p className="barBottom">Notifikasi</p>
            </Nav.Link>
          </Col>

          <Col sm={3}>
            <Nav.Link href="/LengkapiProduk" className='iconBttm'>
              <img src={bttmJual} alt="" style={{paddingLeft:"11%"}}/>
              <p className="barBottom">Jual</p>
            </Nav.Link>
          </Col>

          <Col sm={3}>
            <Nav.Link href="/DaftarJualProduk" className='iconBttm'>
              <img src={bttmDaftar} alt="" style={{paddingLeft:"30%"}}/>
              <p className="barBottom">Daftar Jual</p>
            </Nav.Link>
          </Col>

          <Col sm={3} style={{position:"relative", left:"3%"}}>
            <Nav.Link href="/AkunSaya">
              <img src={bttmAkun} alt="" style={{paddingLeft:"15%"}}/>
              <p className="barBottom">Akun</p>
            </Nav.Link>
          </Col>
          </div>

        </Nav>
      </Navbar>
    )
  };
  
  export default NavbarBottomAkun; 
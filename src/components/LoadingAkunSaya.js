import Skeleton from '@mui/material/Skeleton';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import NavbarNotif from '../components/NavbarNotif';
import Card from 'react-bootstrap/Card';
import NavbarBottomAkun from '../components/NavbarBottomAkun'

import React from 'react'

const LoadingAkunSaya = ({comps}) => {
  return Array (comps)
  .fill(0)
  .map((item, i) => (
    <>
        {/* NAVBAR */}
        <div className="navbarAkunSaya"> <NavbarNotif/> </div>
            <>
             {/* KONTEN */}
              <Container fluid className='pt-5'>
                <Row>
                    <Col md={4}>
                      <div className='titleAkunSaya'>
                        <Skeleton
                        variant="text"
                        />
                        <Skeleton
                        variant="text"/>
                      </div>
                    </Col>
                </Row>

                  {/* Form Lengkapi Profil */}
                  <Row>
                    <Col lg={4}></Col>
                    <Col lg={4} md={6}>
                      
                    <div className='kontenAkunSaya'>
                      <div className='mt-md-6' style={{paddingBottom: '22%'}}>
                              <div className="mt-4" controlId="formBasicProfilePhoto">
                                  <div className={'text-center'}>
                                      <Skeleton
                                      variant="rectangular"
                                      width={96}
                                      height={96}
                                      animation="wave"
                                      className='fotoAkunNew'
                                      style={{ 
                                        position: "relative",
                                        left: "50%"
                                       }}
                                      />
                                    </div>
                              </div>
                        </div>

                          <Card className='cardAkunSaya mb-5'>
                              <div className="mb-3">
                                  <Skeleton
                                  variant="text"
                                  />
                                  <hr/>
                              </div>
                              <div className="mb-3">
                                  <Skeleton
                                  variant="text"
                                  />
                                  <hr/>
                              </div>
                              <div className="mb-3">
                                  <Skeleton
                                  variant="text"
                                  />
                                  <hr className='mb-5'/>
                              </div>
                              </Card>
                            </div>
                      </Col>
                      <Col md={4}></Col>
                  </Row>

                {/* NAVBAR BOTTOM for mobile display */}
                <Row>
                  <div className="bttmNavbarAkun">
                    <NavbarBottomAkun/>
                  </div>
                </Row>
              
                
              </Container>
            </>
       
      </>
  ))
}

export default LoadingAkunSaya
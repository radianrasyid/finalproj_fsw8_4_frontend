import React from "react";
import Skeleton from '@mui/material/Skeleton';
import Nav from "react-bootstrap/Nav";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";

const LoadingSkeletonAccount = ({icons}) => {
    return Array (icons)
    .fill(0)
    .map((item, i) => (
                    <Nav.Link>
                        <Skeleton 
                        variant="rectangular"
                        height={24}
                        width={24}  
                        animation="wave"/>
                    </Nav.Link>
     )
    )
  };
  
  export default LoadingSkeletonAccount; 
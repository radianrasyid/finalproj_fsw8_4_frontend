import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "../Global.css";
import Navbar from "react-bootstrap/Navbar";
import logoSc from "../assets/img/logo.svg";
import Nav from "react-bootstrap/Nav";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import NavDropdown from "react-bootstrap/NavDropdown";
import Button from "react-bootstrap/Button";
import iconLogin from "../assets/img/fi_log-in.svg";
import iconList from "../assets/img/iconList.svg";
import iconNotif from "../assets/img/iconNotif.svg";
import iconUser from "../assets/img/iconUser.svg";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import axios from "axios";
import { useDispatch } from "react-redux";
import { addBarang, getBarangsAsync } from "../redux/barangSlice";
import { useSelector } from "react-redux";
import homeIcon from "../assets/img/home.svg"

const NavbarNotif = (props) => {
  const dispatch = useDispatch();
  const [username, setUsername] = useState("");
  const [notif, setNotif] = useState([]);
  const token = localStorage.getItem("auth_token");
  const barangs = useSelector((state) => state.barangs);
  const [search, setSearch] = useState([]);
  const [query, setQuery] = useState("");

  const fetchUserData = async () => {
    axios.defaults.headers["Authorization"] = `Bearer ${token}`;

    await axios
      .get("https://secondhandkelompok4.herokuapp.com/api/users/data")
      .then(async (response) => {
        let hasil = await response.data;
        setUsername(hasil.user.users.username);
      });
  };

  useEffect(() => {
    setSearch(barangs);
    if (username == "" && token !== "") {
      dispatch(getBarangsAsync());
      fetchUserData();
    }
  });

  const fetchNotif = async () => {
    axios.defaults.headers["Authorization"] = `Bearer ${token}`;

    await axios
      .get("https://secondhandkelompok4.herokuapp.com/api/notifications/read")
      .then(async (respond) => {
        let hasil = await respond.data;
        setNotif(hasil.data);
      });
  };

  useEffect(() => {
    if (username == "" && token !== "") {
      fetchUserData();
      fetchNotif();
    }
  }, []);

  return (
    <>
      <Navbar expand="lg" className="shadowBox" sticky="top" style={{backgroundColor:"white"}}>
        <Container>
          <div className="logoNavbarNotif">
            <Navbar.Brand href="/">
              <img src={logoSc} alt="logo" className="logoNavbarNotif" />
            </Navbar.Brand>
          </div>

          <Nav>
          <NavDropdown
            title={ 
            <Form className="kotakCari">
              <Form.Control
                type="search"
                placeholder="Cari di sini ... 🔎"
                className="searchBox me-2"
                aria-label="Search"
                style={{ width: "115%" }}
                onChange={(e) => setQuery(e.target.value)}
              />
            </Form>
            }
            id="basic-nav-dropdown"
            className="d-none d-lg-block arrow"
            cssClass='e-caret-hide'
            >

      {query.length >= 1 ?
        (search.filter(item => item.nama_barang.toLowerCase().includes(query)).slice(0, 4).map((item) => {
          const link = `https://finalproj-fsw8-4-frontend.vercel.app/your/product/${item.id}`
          let result = []
          
            const words = item.nama_barang.split(" ")
            
          if (words.length >= 3) {
            for (let j = 0; j < 3; j++) {
              result.push(words[j] + " ");
            }
          }
          else{
            for (let i = 0; i < words.length; i++) {
              result.push(words[i] + " ");
            }
          }

          return (
            <>
              <NavDropdown.Item href={link}>
                      <Container>
                        <hr style={{ margin: "auto" }} width="100%" />
                        <Row>
                          <Col md="2" className="p-1">
                            <img
                              src={item.image[0]}
                              alt="iconnotif"
                              style={{
                                width: "100%",
                                height: "50px",
                                objectFit: "contain",
                              }}
                            ></img>
                          </Col>
                          <Col md="10">
                            <h6 className="h2Card txtNotif">
                              {item.categoryid.type}
                            </h6>
                            <h6 className="txtNotif">
                              {result}
                            </h6>
                            <h6 className="txtNotif">Rp {item.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</h6>
                          </Col>
                          <Col md="auto">
                            </Col>
                            </Row>
                          </Container>
                        </NavDropdown.Item>
                </>
              );
            })
          ) : (
            <>
              <NavDropdown.Item>
                <Container>
                  <p>Cari Barang</p>
                </Container>
              </NavDropdown.Item>
            </>
          )}
          
            </NavDropdown>
          </Nav>

          <Navbar.Toggle
            type="button"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasRight"
            aria-controls="offcanvasRight"
            aria-expanded="false"
            aria-label="Toggle navigation"
          />

          {/* different navbar option for each condition */}

          <Navbar.Collapse
            className="offcanvas offcanvas-end justify-content-end"
            tabindex="-1"
            id="offcanvasRight"
            aria-labelledby="offcanvasTopLabel"
          >
              <div className="offcanvas-body navbarKonten">
                  <Nav.Link href="/">
                    <img src={homeIcon} alt="icon" className="menuIcon" />
                    <p className="menuText">Home</p>
                  </Nav.Link>

                <Nav.Link href="/DaftarJualProduk">
                  <img src={iconList} alt="icon" className="menuIcon" />
                  <p className="menuText">Daftar Jual</p>
                </Nav.Link>

                <Nav.Link
                  href="/NotifikasiMobile"
                  className="d-block d-lg-none"
                >
                  <p className="menuText">Notifikasi</p>
                </Nav.Link>

                <NavDropdown
                  title={
                    <img src={iconNotif} alt="icon" className="menuIcon" />
                  }
                  id="basic-nav-dropdown"
                  className="d-none d-lg-block"
                >
                  {notif.length !== 0 ? (
                    notif.slice(0, 2).map((item) => {
                      const words = item.barangid.nama_barang.split(" ");
                      const resultNamaBarang = words[0] + " " + words[1];
                      return (
                        <>
                          <NavDropdown.Item href="/NotifikasiMobile">
                            <Container>
                              <hr style={{ margin: "auto" }} width="100%" />
                              <Row>
                                <Col md="2" className="p-1">
                                  <img
                                    src={item.barangid.image[0]}
                                    alt="iconnotif"
                                    style={{
                                      width: "100%",
                                      height: "50px",
                                      objectFit: "contain",
                                    }}
                                  ></img>
                                </Col>
                                <Col md="10">
                                  <h6 className="h2Card txtNotif">
                                    {item.categoryid.type}
                                  </h6>
                                  <h6 className="txtNotif">
                                    {resultNamaBarang}
                                  </h6>
                                  {item.tawar !== null ? (
                                    <>
                                      <h6 className="txtNotif">Rp {item.barangid.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</h6>
                                      <h6 className="txtNotif">Ditawar Rp {item.tawar.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</h6>
                                      <h6 className="txtNotif">oleh {item.senderid.firstName}</h6>
                                      <h6 className="txtNotif">
                                        <b>STATUS</b> {item.status}
                                      </h6>
                                    </>
                                  ) : (
                                    <>
                                      <h6 className="txtNotif">Rp {item.barangid.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</h6>
                                      <h6 className="txtNotif">
                                        Dibuat oleh {item.senderid.firstName}
                                      </h6>
                                    </>
                                  )}
                                </Col>
                                <Col md="auto">
                                </Col>
                              </Row>
                            </Container>
                          </NavDropdown.Item>
                        </>
                      );
                    })
                  ) : (
                    <>
                      <NavDropdown.Item href="/NotifikasiMobile">
                        <Container>
                          <p>No notif yet</p>
                        </Container>
                      </NavDropdown.Item>
                    </>
                  )}
                </NavDropdown>

                <Nav.Link href="/AkunSaya">
                  <img src={iconUser} alt="icon" className="menuIcon" />
                  <p className="menuText">Akun</p>
                </Nav.Link>
              </div>
           
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default NavbarNotif;

/* Navbar dropdown (lama)
        <NavDropdown
                title={<img src={iconNotif} alt="icon" className="menuIcon" />}
                className="d-none d-lg-block"
              >
                {
                  notif.length !== 0 ? (
                    notif.map((item) => {
                      return(
                        <>
                        <hr style={{ margin: "auto" }} width="100%" />
                        <NavDropdown.Item href="/NotifikasiMobile" className="notifNavbar" style={{backgroundColor:"white"}}>
                          <Container style={{backgroundColor:"white"}}>
                 
                            <Row>
                            <Col md="auto" className="p-1">
                              <img src={item.barangid.image[0]} alt="iconnotif"
                              style={{ 
                                width: "100%",
                                height: "50px",
                                objectFit: "contain"
                               }}></img>
                            </Col>
                            <Col md="auto">
                              <h6 className="h2Card">{item.categoryid.type}</h6>
                              <h6>{item.barangid.nama_barang}</h6>
                              <h6 className="txtNotif">Harga awal {item.barangid.price}</h6>
                              <h6 className="txtNotif">Ditawar Rp {item.tawar}</h6>
                              <h6 className="txtNotif">Ditawar Oleh {item.senderid.fullname}</h6>
                              <h6 className="txtNotif"><b>STATUS</b> {item.status}</h6>
                            </Col>
                            <Col md="auto">
                              <h6 className="h2Card">{Date(item.createdAt)}</h6>
                            </Col>
                          </Row>
                   
                        </Container>
                        
                      
                      </NavDropdown.Item>
                      </>
                        )
                      })
                  ) : (
                      <NavDropdown.Item href="/NotifikasiMobile" className="notifNavbar">
                        <Container className="notifNavbar">
                          <p>No Notif</p>
                        </Container>
                      </NavDropdown.Item>
                      )
                    }

              </NavDropdown>
*/

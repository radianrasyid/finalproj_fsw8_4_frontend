import React, { useEffect } from "react";
import axios from "axios";

function ListOfTodo({ token }) {
  useEffect(() => {
        if(token){
            fetchData(token)
        }
  }, [token]);

  const fetchData = async (token) => {
    await axios.get(
      "https://secondhandkelompok4.herokuapp.com/item/findall", {
        headers: {
            Authorization: `Bearer ${token}`
        }
      })
  };

  return (
    <div>
      <h1>List Of Todo</h1>
    </div>
  );
}

export default ListOfTodo;

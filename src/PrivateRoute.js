import React from "react";
import { Navigate, Route, Outlet } from "react-router-dom";

const PrivateRoute = () => {
  let isToken = localStorage.getItem("auth_token");
  console.log("this", isToken);

  return(
    isToken !== '' && isToken !== null ? <Outlet/> : <Navigate to="/Masuk"/>
  )
}

export default PrivateRoute;

/* import React from "react";
import { Navigate, Route } from "react-router-dom";

function PrivateRoute({ component: Component, ...restOfProps }) {
  const isToken = localStorage.getItem("auth_token");
  console.log("this", isToken);
  
  return (
    <Route
      {...restOfProps}
      render={(props) =>
        isToken !== '' ? <Component {...props} /> : <Navigate to="/Masuk" />
      }
    />
  );
}

export default PrivateRoute; */
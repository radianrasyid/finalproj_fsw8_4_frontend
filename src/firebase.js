
// // TODO: Add SDKs for Firebase products that you want to use
// // https://firebase.google.com/docs/web/setup#available-libraries

import { initializeApp } from "firebase/app";
import { api } from './api'
import {
 GoogleAuthProvider,
 getAuth,
 signInWithPopup,
 signInWithEmailAndPassword,
 createUserWithEmailAndPassword,
 sendPasswordResetEmail,
 signOut,
 sendEmailVerification,
 updatePassword,
} from "firebase/auth";

import {
 getFirestore,
 query,
 getDocs,
 collection,
 where,
 addDoc,
} from "firebase/firestore";

import { useNavigate } from 'react-router-dom'

// // Your web app's Firebase configuration
// // For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAIfClgYmSNXex6jsttIXeSbsYA8X5gA1A",
  authDomain: "radianprojies.firebaseapp.com",
  databaseURL: "https://radianprojies-default-rtdb.firebaseio.com",
  projectId: "radianprojies",
  storageBucket: "radianprojies.appspot.com",
  messagingSenderId: "890314411848",
  appId: "1:890314411848:web:cfdd3bc496d623a7d62d7d",
  measurementId: "G-8C82QTZ56E",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth();
const db = getFirestore();

// auth.setPersistence(browserLocalPersistence);

api.interceptors.request.use(async (config) => {
  if (!auth.currentUser.isAnonymous) {
    config.headers["authorization"] = await auth.currentUser.getIdToken();
  }

  return config;
});

const googleProvider = new GoogleAuthProvider();
const signInWithGoogle = async () => {
  try {
    await signInWithPopup(auth, googleProvider)
    .then(async (res)=> {
      await api.post("https://secondhandkelompok4.herokuapp.com/api/users/registeruser", {
      email: res.user.email,
      fullname: res.user.displayName,
      username: res.user.email
    })
    .then(async(respond) => {
      await api
        .post("https://secondhandkelompok4.herokuapp.com/api/users/login", {
          email: res.user.email,
          password: respond.data.pass,
        })
        .then(async (respond) => {
          if (respond.data.token) {
            localStorage.setItem("auth_token", respond.data.token);
            alert("Create account berhasil, silahkan cek email anda")
          } else {
            alert("invalid");
          }
        });
    })
    });
  } catch (error) {
    console.log(error);
    alert(error.message)
  }

};

const loginWithEmailAndPassword = async (email, password) => {
  try {
    await signInWithEmailAndPassword(auth, email, password);
  } catch (error) {
    console.log(error);
    alert(error.message);
  }
};

const registerWithEmailAndPassword = async (name, email, password) => {
  try {
    await createUserWithEmailAndPassword(auth, email, password).then(
      async (res) => {
        console.log(res.user);
        api.post(
          "https://secondhandkelompok4.herokuapp.com/api/users/registeruser",
          {
            fullname: name,
            email: email,
            password: password,
          }
        );
      }
    );
  } catch (error) {
    console.log(error);
    alert(error.message);
  }
};

const sendPasswordReset = async (email) => {
  try {
    await sendPasswordResetEmail(auth, email).then(() => {
      alert("Kindly check your email to proceed your password reset");
    });
  } catch (error) {
    console.log(error);
    alert(error.message);
  }
};

const emailVerif = async () => {
  sendEmailVerification(auth.currentUser).then(() => {
    alert("Email Verification Sent!");
  });
};

const logout = async () => {
  signOut(auth);
};

// const storage = getStorage(app);

export {
  auth,
  signInWithGoogle,
  loginWithEmailAndPassword,
  registerWithEmailAndPassword,
  sendPasswordReset,
  logout,
  // storage,
  // analytics,
  emailVerif,
};

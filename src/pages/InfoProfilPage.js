import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import { useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import '../Global.css';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import ReactLoading from 'react-loading'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonLargePurp from '../components/ButtonLargePurp';
import NavbarPlain from '../components/NavbarPlain';
import { ArrowLeft } from 'react-bootstrap-icons';
import inputPhotoProfile from '../assets/img/inputPhotoProfile.svg';
import axios from "axios";
import { useNavigate } from "react-router-dom";

function InfoProfilPage() {

  // API

    // validasi untuk tiap field yang kosong
    const [validate, setValidate] = useState(false);
    
    // data
    const [username,setUsername] = useState('');
    const [address, setAddress] = useState('');
    const [city, setCity] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [firstname, setFirstName] = useState('');
    const [lastname, setLastName] = useState('');
    const [fullname, setFullName] = useState('');
    const [file, setFile] = useState()
    const [preview, setPreview] = useState()
    const [gambar, setGambar] = useState();
    const direct = useNavigate();
    const [Msg, setMsg] = useState("");

    let token = localStorage.getItem("auth_token")

    useEffect(() => {
        if(username == ''){
          fetchUserData();
          return;
        }
    }, [])

    const fetchUserData = async () => {
      setLoading(true)
      axios.defaults.headers['Authorization'] = `Bearer ${token}`

      await axios.get("https://secondhandkelompok4.herokuapp.com/api/users/data")
      .then(async(response) => {
        let hasil = await response.data;
        setUsername(hasil.user.users.username);
        setAddress(hasil.user.users.address);
        setCity(hasil.user.users.city);
        setPhoneNumber(hasil.user.users.phoneNumber);
        setFirstName(hasil.user.users.firstName);
        setLastName(hasil.user.users.lastName);
        setFullName(hasil.user.users.fullname);
        setGambar(hasil.user.users.image)
      })
      setLoading(false)
    }

    const saveFile = (e) => {
      setFile(e.target.files[0])
    }

    async function changeNumber(){
      let set = phoneNumber.slice(1);
      let newNumber = "62" + set;
      setPhoneNumber(newNumber)
    }
    
    const submitForm = async(e) => {
      e.preventDefault();

      const formData = new FormData();
  
      formData.append("firstname", firstname);
      formData.append("lastname", lastname);
      formData.append("fullname", fullname);
      formData.append("username", username);
      formData.append("address", address);
      formData.append("nomorhp", "62" + phoneNumber.slice(1));
      formData.append("city", city);
      formData.append("picture", file);
  
      try{
        axios.defaults.headers['Authorization'] = `Bearer ${token}`
        setLoading(true)
        await axios.put("https://secondhandkelompok4.herokuapp.com/api/users/update",
        formData)
        .then((response) => {
          setLoading(false)
          window.location.reload()
        })
        alert('update berhasil')
      }catch(error){
        console.log(error);
      }
    }

    // dropzone

     const img = { display: 'block', width: 'auto', height: '100%' };

     const [files, setFiles] = useState([]);
     const [loading, setLoading] = useState(false)
     const {getRootProps, getInputProps} = useDropzone({
       accept: {
         'image/*': []
       },
       onDrop: acceptedFiles => {
         setFiles(acceptedFiles.map(file => Object.assign(file, {
           preview: URL.createObjectURL(file)
         })));
       }
     },
     console.log(files));
     
     const thumbs = files.map(file => (
       <div className='thumb' key={file.name}>
         <div className='thumbInner'>
           <img src={file.preview} style={img} alt="productImgNew"
             onLoad={() => { URL.revokeObjectURL(file.preview) }}
           />
         </div>
       </div>
     ));

  const hiddenFileInput = React.useRef(null);

  const handleClick = e => {
    hiddenFileInput.current.click()
  }

  const handleChange = async(e) => {
    const set = await URL.createObjectURL(e.target.files[0])
    setPreview(set)
    setFile(e.target.files[0])
  }

  // page
    return (
      <>
        {/* NAVBAR */}
        <NavbarPlain judulPage="Lengkapi Info Akun"/>

        {/* KONTEN */}
        <Container fluid className='pt-5'>

            <Row>
              <Col md={3}>
                <a className='iconArrowLeft' href='/AkunSaya' style={{ color: "black" }}>
                  <ArrowLeft size={34} className=''/>
                </a>
              </Col>
            </Row>

            {/* Form Lengkapi Profil */}
            <Row>
              <Col lg={4}></Col>
              <Col lg={4} md={6}>
                <div className='formInfoProfil'>
                  {
                    loading === false ? (
                      <Form className='mt-md-6 mb-4' style={{paddingBottom: '22%'}} 
                      validated={validate} 
                      onSubmit={submitForm}
                      encType="multipart/form-data">

                        <p style={{textAlign:'right', color:'#4b1979'}} className="mt-4"><b>* Wajib diisi</b></p>

                        <Form.Group className="mb-3 mt-4" controlId="formBasicProfilePhoto">
                          <div className="text-center">
                          <input type="file" id='picture' onChange={handleChange} style={{
                            width: "40em",
                            height: "3em",
                            marginTop: "2em",
                            position: "absolute",
                            display: "none"
                          }}
                          ref={hiddenFileInput}
                          />
                          <button type="button" onClick={handleClick} style={{ 
                            border: "none",
                            color: "white",
                            backgroundColor: "white"
                           }}>
                            { gambar == null || gambar == undefined || gambar == "" ? (
                              <img src={
                                preview == null || preview == undefined || preview == "" ? (
                                  inputPhotoProfile
                                ) : (
                                  preview
                                )
                              } alt="none" className='fotoAkunNew'/>
                              ) : (
                              <img src={gambar} alt="foto" className='fotoAkunNew'/>
                              )
                            }
                          </button>
                              <aside className='thumbsContainer' style={{position:'relative', left:'38%'}}>
                                {thumbs}
                              </aside>
                          </div>
                        </Form.Group>

                        <Form.Group className="mb-3 mt-4" controlId="formBasicName">
                          <Form.Label>Username <a style={{color:'#4b1979'}}>*</a></Form.Label>
                          <Form.Control 
                            type="text" 
                            id="username" 
                            placeholder={username} 
                            className="formRounded"
                            value={username}
                            onChange={(e) => setUsername(e.target.value)} 
                            required />
                        </Form.Group>

                        <Form.Group className="mb-3 mt-4" controlId="formBasicName">
                          <Form.Label>First Name <a style={{color:'#4b1979'}}>*</a></Form.Label>
                          <Form.Control 
                            type="text" 
                            id="firstname" 
                            placeholder={firstname} 
                            className="formRounded"
                            value={firstname}
                            onChange={(e) => setFirstName(e.target.value)} 
                            required />
                        </Form.Group>

                        <Form.Group className="mb-3 mt-4" controlId="formBasicName">
                          <Form.Label>Last Name <a style={{color:'#4b1979'}}>*</a></Form.Label>
                          <Form.Control 
                            type="text" 
                            id="lastname" 
                            placeholder={lastname} 
                            className="formRounded"
                            value={lastname}
                            onChange={(e) => setLastName(e.target.value)} 
                            required />
                        </Form.Group>

                        <Form.Group className="mb-3 mt-4" controlId="formBasicName">
                          <Form.Label>Full Name <a style={{color:'#4b1979'}}>*</a></Form.Label>
                          <Form.Control 
                            type="text" 
                            id="fullname" 
                            placeholder={fullname} 
                            className="formRounded"
                            value={fullname}
                            onChange={(e) => setFullName(e.target.value)} 
                            required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicCity">
                          <Form.Label>Kota <a style={{color:'#4b1979'}}>*</a></Form.Label>
                          <Form.Select 
                            type="text"  
                            id="city"
                            placeholder={city}
                            className="formRounded" 
                            value={city}
                            onChange={(e) => setCity(e.target.value)}
                            required>
                              <option value="" selected>Pilih Kota</option>
                              <option value="Batam">Batam</option>
                              <option value="Surabaya">Surabaya</option>
                              <option value="Lombok">Lombok</option>
                              <option value="Medan">Medan</option>
                              <option value="Jakarta">Jakarta</option>
                              <option value="Bandung">Bandung</option>
                              <option value="Semarang">Semarang</option>
                              <option value="Solo">Solo</option>
                              <option value="Kediri">Kediri</option>
                              <option value="Padang">Padang</option>
                              <option value="Manado">Manado</option>
                              <option value="Madiun">Madiun</option>
                              <option value="Denpasar">Denpasar</option>
                          </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicAddress">
                          <Form.Label>Alamat <a style={{color:'#4b1979'}}>*</a></Form.Label>
                          <Form.Control
                            type="textarea"  
                            id="alamat" 
                            placeholder={address} 
                            className="formRounded inputLarge" 
                            value={address}
                            onChange={(e) => setAddress(e.target.value)} 
                            required />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicHP">
                          <Form.Label>Nomer Handphone <a style={{color:'#4b1979'}}>*</a></Form.Label>
                          <Form.Control
                            type="text"  
                            id="phoneNumber" 
                            placeholder={phoneNumber} 
                            className="formRounded"
                            value={phoneNumber}
                            onChange={async(e) => {
                              setPhoneNumber(e.target.value);
                            }} 
                            required />
                        </Form.Group>

                        <ButtonLargePurp namaButton="Simpan" linkHref="/"/>
                    </Form>
                    ) : (
                      <>
                      <div className="text-center loadingBar">
                        <ReactLoading type="bars" color="#7126b5"
                          height={120} width={70} />
                      </div>
                      <h5 className='text-center' style={{color:"#a06ece"}}><b>Loading...</b></h5>
                      </>
                    )
                  }
                  </div>
                </Col>
                <Col md={4}></Col>
            </Row>
        </Container>
      </>
    );
  };

  export default InfoProfilPage;
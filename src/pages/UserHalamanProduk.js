import { useState, useEffect } from "react";
import "../style.css"
import axios from "axios";
import { useParams } from "react-router-dom";
import Form from "react-bootstrap/Form";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Carousel from "react-bootstrap/Carousel";
import { WhatsApp, Delete, Edit } from "@mui/icons-material";
import ReactLoading from 'react-loading'
import iconSeller1 from "../assets/img/iconSeller.png";
import NavbarHome from "../components/NavbarHome";
import iconLogin from "../assets/img/fi_log-in.svg";
import { getIdTokenResult } from "firebase/auth";
import LoadingPart from "../components/LoadingPart";
import { useNavigate } from 'react-router-dom'
import { Windows } from "react-bootstrap-icons";
import { useSelector } from "react-redux";

function UserHalamanProduk() {
  const [post, setPost] = useState({});
  const [sell, setSell] = useState({})
  const [nego, setNego] = useState("");
  const [list, setList] = useState({});
  const [user, setUser] = useState({})
  const [currentUser, setCurrentUser] = useState({})
  const [loading, setLoading] = useState(false)
  let { id } = useParams();

  const tokenRedux = useSelector((state) => state.auth.token)

  const token = localStorage.getItem("auth_token")

  const direct = useNavigate();

  const fetchUserData = async() => {
    setLoading(true)
    axios.defaults.headers['Authorization'] = `Bearer ${token}`
    await axios.get(`https://secondhandkelompok4.herokuapp.com/api/users/data`)
    .then((res) => {
      setCurrentUser(res.data.user.users)
      console.log("INI DATA USER", res.data.user.users)
    })
  }

  const fetchData = async (value) => {
    setLoading(true)
    await axios
      .get(`https://secondhandkelompok4.herokuapp.com/api/items/findone/${value}`)
      .then((res) => {  
        setPost(res.data.barang[0]);
        setLoading(false)
      })
      .catch((err) => {
        console.log(err);
        setLoading(false)
      });
  }

  const fetchNego = async(value) => {
    setLoading(true)
    await axios.get(`https://secondhandkelompok4.herokuapp.com/api/negos/findfor/${value}`)
    .then((res) => {
      setList(res.data.data[0])
      setUser(res.data.data[0].receiverid)
    })
    setLoading(false)
  } 

  const fetchUser = async() => {
    setLoading(true)
    axios.defaults.headers['Authorization'] = `Bearer ${token}`

    await axios.get("https://secondhandkelompok4.herokuapp.com/api/users/data")
    .then(async(res) => {
      let hasil = res.data;
      setSell(hasil.user.users)
    })
    setLoading(false)
  }

  const createNego = async(e) => {
    e.preventDefault();

    const data = currentUser;

    if(data.address == "" || null || data.city == "" || null || data.email == "" || null || data.username == "" || null || 
      data.phoneNumber == "" || null){
        alert("Lengkapi dulu yuk profilmu")
        direct("/LengkapiProfil")
        return;
      }else if(!data.address){
        direct("/masuk")
      }
    
    if(Number(nego)<=post.price && Number(nego)>((post.price)-(post.price * 0.25))){
      axios.defaults.headers['Authorization'] = `Bearer ${token}`;
      await axios.get("https://secondhandkelompok4.herokuapp.com/api/users/data")
      .then(async (respond) => {
        let hasil = respond.data;
        if(hasil.user.users.id === post.postedby.id){
          alert("Ini produkmu sendiri bestie...")
          return;
        }
        setLoading(true);
        await axios.post(`https://secondhandkelompok4.herokuapp.com/api/negos/create/${id}`, {
        tawar: nego
      }).then(()=>{
        setLoading(false)
        window.location.reload()
      })
      })
    }else if(Number(nego) > post.price || Number(nego) > ((post.price) - (post.price * 0.25))){
      alert("Waduh harganya kemahalan tuh, atau terlalu murah...")
    }
  }

  const updateNego = async(e) => {
    e.preventDefault();

    if(Number(nego)<=post.price && Number(nego)>((post.price) - (post.price * 0.25))){
      setLoading(true)
      axios.defaults.headers['Authorization'] = `Bearer ${token}`
      await axios.put(`https://secondhandkelompok4.herokuapp.com/api/negos/updatereject/${id}`, {
        tawar: nego
      }).then(() => {
        setLoading(false)
        window.location.reload()
      })
    }
    else{
      alert("kasih harga yang masuk akal dong...")
    }
  }

  const directBuy = async(e) => {
    e.preventDefault();

    setLoading(true)
    axios.defaults.headers['Authorization'] = `Bearer ${token}`
    await axios.post(`https://secondhandkelompok4.herokuapp.com/api/negos/directbuy/${id}`)
    .then(() => {
      setLoading(false)
      window.location.reload()
    })
  }

  const updateDirectBuy = async(e) => {
    e.preventDefault();

    setLoading(true);
    axios.defaults.headers['Authorization'] = `Bearer ${token}`
    await axios.put(`https://secondhandkelompok4.herokuapp.com/api/negos/directbuyupdate/${id}`)
    .then(()=>{
      setLoading(false);
      window.location.reload()
    })
  }

  useEffect(() => {
      fetchData(id);
      fetchUser()
      fetchNego(id)
      fetchUserData()
  }, []);

  const link = `https://wa.me/${user.phoneNumber}`
  const linkedit = `https://finalproj-fsw8-4-frontend.vercel.app/editproduct/${post.id}`
  const linkgominat = `https://finalproj-fsw8-4-frontend.vercel.app/DaftarJualDiminati`

  if(loading === true){
    return(
      <>
        {/* Page loading */}
        <NavbarHome />
        <LoadingPart/>
    </>
    )
  }
  if(post || sell || list){
    return (
      <>
        {/* NAVBAR */}
        <NavbarHome />
  
        {/* PRODUCT */}
        <Container>
          <div class="row mt-4">
            <div class="col-lg-8">
              <Carousel>
                {
                  post.image ? (
                    post.image.map((item)=>{
                      return(
                        <Carousel.Item style={{
                          borderRadius: "8px"
                        }}>
                          <img
                            className="d-block w-100"
                            src={item}
                            alt="First slide"
                            style={{ 
                              height: "27rem",
                              objectFit: "contain",
                              borderRadius: "8px"
                             }}
                          />
                        </Carousel.Item>
                      )
                    })
                  ):(
                    <p>No image</p>
                  )
                }
              </Carousel>
            </div>
            {/* COL KE 2 */}
            <div class="col-lg-4">
              <div class="textShadowBox p-4">
                <h4>{post.nama_barang}</h4>
                <h6>Harga :</h6>
                <h5>Rp {post.price ? post.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : post.price}</h5>
                <div>
                  {
                    post.postedBy  !== sell.id && list ? (
                      list.sendId == sell.id ? (
                        list.isAccept == "ACCEPT" ? (
                          post.isTerjual === "TERJUAL" ? (
                            <Button className="btnPurple w-100 mt-2 mb-2"
                              style={{ 
                                backgroundColor: "#25D366"
                              }}
                              disabled={true}
                              >
                                PEMBELIAN BERHASIL!
                            </Button>
                          ) : (
                            <>
                              <a href={link}
                               target="_blank">
                                <Button className="btnPurple w-100 mt-2 mb-2"
                                  style={{ 
                                  backgroundColor: "#25D366"
                                  }}
                                  >
                                  <WhatsApp style={{ 
                                    marginRight: "1rem",
                                    marginBottom: "0.2rem"
                                  }} />
                                  Hubungi Penjual
                                </Button>
                              </a>
                            </>
                          )
                        ) : (
                          list.isAccept == "REJECT" ? (
                            <>
                              <p className="mt-3" style={{fontSize:"smaller", color:"grey"}}>
                                Ups, tawaranmu sebelumnya belum cocok nih. Masih mau beli?
                              </p>
                              <Button className="btnPurple w-100 mb-2"
                              type="button"
                              data-bs-toggle="modal" 
                              data-bs-target="#exampleModal">
                                Coba Nego Lagi!</Button>
                                <Form onSubmit={updateDirectBuy}>
                                <Button
                                type="submit"
                                className="btnPurpleSc w-100 mt-2 mb-2"
                                >
                                  Gas Beli, Tanpa Nego!
                                </Button>
                                </Form>
                               <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div className="modal-dialog">
                                  <div className="modal-content">
                                    <div className="modal-header">
                                      <h5 className="modal-title" id="exampleModalLabel">Saatnya Nego!</h5>
                                      <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                    {
                                      loading === true ? (
                                        <>
                                          <div className="text-center loadingBar">
                                            <ReactLoading type="bars" color="#7126b5"
                                              height={120} width={70} />
                                          </div>
                                          <h5 className='text-center' style={{color:"#a06ece"}}><b>Loading...</b></h5>
                                        </>
                                      ) : (
                                        <Form onSubmit={updateNego}>
                                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                            <Form.Label>Harga tawaranmu akan diketahui penjual. Jika cocok, kamu akan segera dihubungi penjual</Form.Label>
                                            <div
                                                style={{
                                                  background: "#EEEEEE",
                                                  boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.15)",
                                                  borderRadius: "16px",
                                                }}
                                                className="my-1"
                                              >
                                                <div className="row pt-2">
                                                  <div className="col-2 p-3">
                                                    <img src={post.image[0]} alt="iconseller" className="fotoAkunSmallVer"/>
                                                  </div>
                                                  <div className="col-10 p-3" style={{paddingLeft:"1em"}}>
                                                    <h6><b>{post.nama_barang}</b></h6>
                                                    <p>Rp {post.price ? post.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : post.price}</p>
                                                  </div>
                                                </div>
                                              </div>
                                            <p className="mt-3"><b>Harga Tawar</b></p>
                                            <Form.Control
                                              required
                                              value={nego}
                                              onChange={(e) => setNego(Number(e.target.value))}
                                              type="text"
                                              placeholder="Masukkan tawaranmu"
                                              className="formRounded"
                                            />
                                          </Form.Group>
                                          <p className="mt-2"><b>Note :</b> Masukkan angka saja</p>
                                          <div class="modal-footer">
                                              <button type="submit" className="btn btnPurple" data-bs-dismiss="modal" aria-label="Close">Kirim!</button>
                                          </div>
                                        </Form>
                                      )
                                    }
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </>
                          ) : (
                            token == "" || token == undefined || token == "" ? (
                              <a href="/masuk">
                                <Button className="btnPurple w-100 mt-2 mb-2">
                                <img src={iconLogin} alt="logo" className="logoNavbarNotif" />
                                  Masuk untuk nego
                                </Button>
                              </a>
                            ) : (
                              post.isTerjual === "TERJUAL" ? (
                                <Button className="btnPurple w-100 mt-2 mb-2"
                              style={{ 
                                backgroundColor: "#f74343"
                              }}
                              disabled={true}
                              >
                               <b>SUDAH TERJUAL</b>
                            </Button>
                              ) : (
                                <Button className="btnPurple w-100 mt-2 mb-2"
                              disabled={true}
                              style={{ 
                              backgroundColor: "gray"
                              }}>
                                Menunggu Respon Penjual
                              </Button>
                              )
                            )
                            )
                          )
                      ) : (
                        post.isTerjual === "TERJUAL" ? (
                          <Button className="btnPurple w-100 mt-2 mb-2"
                              style={{ 
                                backgroundColor: "#f74343"
                              }}
                              disabled={true}
                              >
                                <b>SUDAH TERJUAL</b>
                            </Button>
                        ) : (
                          <>
                        <Button className="btnPurple w-100 mt-2 mb-2"
                        type="button"
                        data-bs-toggle="modal" 
                        data-bs-target="#exampleModal">
                          Nego Dong!
                        </Button>
                        <Form onSubmit={directBuy}>
                        <Button
                        type="submit"
                        className="btnPurpleSc w-100 mt-2 mb-2"
                        >
                          Gas Beli, Tanpa Nego!
                        </Button>
                        </Form>
                          <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div className="modal-dialog">
                            <div className="modal-content">
                              <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Saatnya Nego!</h5>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>
                              <div className="modal-body">
                              {
                                loading === true ? (
                                  <>
                                    <div className="text-center loadingBar">
                                      <ReactLoading type="bars" color="#7126b5"
                                        height={120} width={70} />
                                    </div>
                                    <h5 className='text-center' style={{color:"#a06ece"}}><b>Loading...</b></h5>
                                  </>
                                ) : (
                                    <Form onSubmit={createNego}>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                    <Form.Label>Harga tawaranmu akan diketahui penjual. Jika cocok, kamu akan segera dihubungi penjual</Form.Label>
                                    <div
                                        style={{
                                          background: "#EEEEEE",
                                          boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.15)",
                                          borderRadius: "16px",
                                        }}
                                        className="my-1"
                                      >
                                        <div className="row pt-2">
                                          <div className="col-2 p-3">
                                            <img src={post.image[0]} alt="iconseller" className="fotoAkunSmallVer"/>
                                          </div>
                                          <div className="col-10 p-3" style={{paddingLeft:"1em"}}>
                                            <h6><b>{post.nama_barang}</b></h6>
                                            <p>Rp {post.price ? post.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : post.price}</p>
                                          </div>
                                        </div>
                                      </div>
                                    <p className="mt-3"><b>Harga Tawar</b></p>
                                    <Form.Control
                                      required
                                      value={nego}
                                      onChange={(e) => setNego(Number(e.target.value))}
                                      type="text"
                                      placeholder="Masukkan tawaranmu"
                                      className="formRounded"
                                    />
                                  </Form.Group>
                                  <p className="mt-2"><b>Note :</b> Masukkan angka saja</p>
                                  <div class="modal-footer">
                                      <button type="submit" className="btn btnPurple" data-bs-dismiss="modal" aria-label="Close">Kirim!</button>
                                  </div>
                                </Form>
                                )
                              }
                              </div>
                            </div>
                          </div>
                        </div>
                        </>
                        )
                      )
                    ) : (
                      sell.id == post.postedBy ? (
                        post.isTerjual === "TERJUAL" ? (
                          <Button className="btnPurple w-100 mt-2 mb-2"
                              style={{ 
                                backgroundColor: "#f74343"
                              }}
                              disabled={true}
                              >
                                <b>ITEM BERHASIL DIJUAL</b>
                            </Button>
                        ) : (
                          <>
                          <div>
                            <a href={linkgominat}>
                            <Button className="btnPurple w-100 mt-2 mb-2"
                              type="button">
                              Cek Status Barang
                            </Button>
                            </a>
                          </div>
                          <div>
                            <a href={linkedit}>
                            <Button className="btnPurpleSc w-100 mt-2 mb-2"
                              type="button">
                              <Edit style={{ marginRight: "0.5rem", marginBottom: "0.2rem"}} />
                              Edit Barang
                            </Button>
                            </a>
                          </div>
                          <div>
                              <Button className="btnRedSc w-100 mt-2 mb-2"
                                type="button">
                                <Delete style={{ marginRight: "0.5rem", marginBottom: "0.2rem"}} />
                                Hapus Barang
                              </Button>
                           </div>
                        </>
                        )
                      ) : (
                        post.isTerjual === "TERJUAL" ? (
                          <Button className="btnPurple w-100 mt-2 mb-2"
                              style={{ 
                                backgroundColor: "#f74343"
                              }}
                              disabled={true}
                              >
                                <b>SUDAH TERJUAL</b>
                            </Button>
                        ) : (
                          <>
                        <Button className="btnPurple w-100 mt-2 mb-2"
                        type="button"
                        data-bs-toggle="modal" 
                        data-bs-target="#exampleModal">
                          Nego Dong!
                        </Button>
                        <Form onSubmit={directBuy}>
                        <Button
                        type="submit"
                        className="btnPurpleSc w-100 mt-2 mb-2"
                        >
                          Gas Beli, Tanpa Nego!
                        </Button>
                        </Form>
                          <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div className="modal-dialog">
                            <div className="modal-content">
                              <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Saatnya Nego!</h5>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>
                              <div className="modal-body">
                              {
                                loading === true ? (
                                  <>
                                    <div className="text-center loadingBar">
                                      <ReactLoading type="bars" color="#7126b5"
                                        height={120} width={70} />
                                    </div>
                                    <h5 className='text-center' style={{color:"#a06ece"}}><b>Loading...</b></h5>
                                  </>
                                ) : (
                                    <Form onSubmit={createNego}>
                                    <Form.Group className="mb-3" controlId="formBasicEmail">
                                    <Form.Label>Harga tawaranmu akan diketahui penjual. Jika cocok, kamu akan segera dihubungi penjual</Form.Label>
                                    <div
                                        style={{
                                          background: "#EEEEEE",
                                          boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.15)",
                                          borderRadius: "16px",
                                        }}
                                        className="my-1"
                                      >
                                        <div className="row pt-2">
                                          <div className="col-2 p-3">
                                            <img src={post.image[0]} alt="iconseller" className="fotoAkunSmallVer"/>
                                          </div>
                                          <div className="col-10 p-3" style={{paddingLeft:"1em"}}>
                                            <h6><b>{post.nama_barang}</b></h6>
                                            <p>Rp {post.price ? post.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : post.price}</p>
                                          </div>
                                        </div>
                                      </div>
                                    <p className="mt-3"><b>Harga Tawar</b></p>
                                    <Form.Control
                                      required
                                      value={nego}
                                      onChange={(e) => setNego(Number(e.target.value))}
                                      type="text"
                                      placeholder="Masukkan tawaranmu"
                                      className="formRounded"
                                    />
                                  </Form.Group>
                                  <p className="mt-2"><b>Note :</b> Masukkan angka saja</p>
                                  <div class="modal-footer">
                                      <button type="submit" className="btn btnPurple" data-bs-dismiss="modal" aria-label="Close">Kirim!</button>
                                  </div>
                                </Form>
                                )
                              }
                              </div>
                            </div>
                          </div>
                        </div>
                        </>
                        )
                      )
                    )
                  }
                </div>
              </div>
  
              <div class="textShadowBox p-4 mt-4">
                <div className="row justify-content-start">
                  <div className="col-2">
                    <img src={post.postedby ? post.postedby.image ? post.postedby.image : iconSeller1 : iconSeller1}
                    style={{
                      height: "50px",
                      width: "100%",
                      objectFit: "cover",
                      borderRadius: "8px"
                    }}
                    alt="iconseller" />
                  </div>
                  <div className="col-8">
                    <h5>{post.postedby ? post.postedby.fullname : "Penjual"}</h5>
                    <h6>{post.postedby ? post.postedby.city : "kota"}</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
  
          {/* TEXT DESCRIPSI */}
          <div class="row">
            <div class="col-lg-8">
              <div class="textShadowBox p-4 mt-4 mb-4">
                <h4>Deskripsi</h4>
                <div align="justify">{post.description}</div>
              </div>
            </div>
          </div>
        </Container>
      </>
    );
  }else if(post.id){
    return (
      <>
        {/* NAVBAR */}
        <NavbarHome />
  
        {/* PRODUCT */}
        <Container>
          <div class="row mt-4">
            <div class="col-lg-8">
              <Carousel>
                {
                  post.image ? (
                    post.image.map((item)=>{
                      return(
                        <Carousel.Item>
                          <img
                            className="d-block w-100"
                            src={item}
                            alt="First slide"
                          />
                        </Carousel.Item>
                      )
                    })
                  ):(
                    <p>No image</p>
                  )
                }
              </Carousel>
            </div>
            {/* COL KE 2 */}
            <div class="col-lg-4">
              <div class="textShadowBox p-4">
                <h4>{post.nama_barang}</h4>
                <h6>Harga :</h6>
                <h5>Rp {post.price ? post.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : post.price}</h5>
                <div>
                <Button className="btnPurple w-100 mt-2 mb-2"
                        type="button"
                        data-bs-toggle="modal" 
                        data-bs-target="#exampleModal">
                          Nego Dong!
                        </Button>
                        <Form onSubmit={directBuy}>
                        <Button
                        type="submit"
                        className="btnPurpleSc w-100 mt-2 mb-2"
                        >
                          Gas Beli, Tanpa Nego!
                        </Button>
                        </Form>
                        <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div className="modal-dialog">
                            <div className="modal-content">
                              <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Kayanya kamu belum login nih..</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>
                              <div class="modal-body">
                              {
                                loading === true ? (
                                  <>
                                    <div className="text-center loadingBar">
                                      <ReactLoading type="bars" color="#7126b5"
                                        height={120} width={70} />
                                    </div>
                                    <h5 className='text-center' style={{color:"#a06ece"}}><b>Loading...</b></h5>
                                  </>
                                ) : (
                                  <Form onSubmit={createNego}>
                                  <Form.Group className="mb-3" controlId="formBasicEmail">
                                  <Form.Label className="text-center">Mau beli ini? <i>log in</i> dulu yuk!</Form.Label>
                                  <div
                                        style={{
                                          background: "#EEEEEE",
                                          boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.15)",
                                          borderRadius: "16px",
                                        }}
                                        className="my-1"
                                      >
                                        <div className="row pt-2">
                                          <div className="col-2 p-3">
                                            <img src={post.image[0]} alt="iconseller" className="fotoAkunSmallVer"/>
                                          </div>
                                          <div className="col-10 p-3" style={{paddingLeft:"1em"}}>
                                            <h6><b>{post.nama_barang}</b></h6>
                                            <p>Rp {post.price ? post.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : post.price}</p>
                                          </div>
                                        </div>
                                    </div>
                                </Form.Group>
                                <div className="modal-footer text-center">
                                  <a href="/Masuk">
                                    <button type="button" className="btn btnPurple">
                                    <img src={iconLogin} alt="logo" className="logoNavbarNotif" />
                                      Masuk
                                    </button>
                                    </a>
                                </div>
                              </Form>
                                )
                              }
                              </div>
                            </div>
                          </div>
                        </div>
                </div>
              </div>
  
              <div class="textShadowBox p-4 mt-4">
                <div className="row justify-content-start">
                  <div className="col-2">
                    <img src={post.postedby ? post.postedby.image : {iconSeller1}}
                    style={{
                      height: "50px",
                      width: "100%",
                      objectFit: "cover",
                      borderRadius: "8px"
                    }}
                    alt="iconseller" />
                  </div>
                  <div className="col-8">
                    <h5>{post.postedby ? post.postedby.fullname : "Penjual"}</h5>
                    <h6>{post.postedby ? post.postedby.city : "kota"}</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
  
          {/* TEXT DESCRIPSI */}
          <div class="row">
            <div class="col-lg-8">
              <div class="textShadowBox p-4 mt-4 mb-4">
                <h4>Deskripsi</h4>
                <div align="justify">{post.description}</div>
              </div>
            </div>
          </div>
        </Container>
      </>
    );
  }
}

export default UserHalamanProduk;
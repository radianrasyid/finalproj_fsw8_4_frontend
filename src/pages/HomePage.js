import React, { useEffect, useState } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import Carousel from "react-bootstrap/Carousel";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import NavbarHome from "../components/NavbarHome";
import carousel1 from "../assets/img/imgbanner.svg";
import iconSearch from "../assets/img/fi_search.svg";
import gift from "../assets/img/gift.svg";
import { useDispatch } from "react-redux";
import { useSelector } from 'react-redux'
import "../style.css";
import "../Global.css";
import LoadingSkeletonCard from "../components/LoadSkeletonCard";
import { selectCurrentToken, selectCurrentUser } from "../redux/authSlice"
import { setCredentials } from "../redux/authSlice"

function HomePage({ tokenize }) {
  // API data barang

  const token = localStorage.getItem("auth_token")
  const barangs = useSelector((state) => state.barangs)
  const direct = useNavigate();
  const [listBarang, setListBarang] = useState([]);
  const dispatch = useDispatch();

  let {id} = useParams();
  const [barang, setBarang] = useState([]);
  const [user, setUser] = useState([]);

  useEffect(() => {
    setListBarang(barangs)
    if(barang.length == 0){
      fetchDataNoToken()
      fetchUserData(token);
      return;
    }
    else if (barang.length == 0 && token == null || undefined || "") {
      fetchDataNoToken();
      return;
    }
    
  }, [barang]);

  const fetchDataNoToken = async (token) => {
    axios.defaults.headers["Authorization"] = `Bearer ${token}`;
    await axios
      .get("https://secondhandkelompok4.herokuapp.com/api/items/findall")
      .then(async (response) => {
        let hasil = await response.data;
        setBarang(hasil);
      })
  };

  const fetchUserData = async (token) => {
    axios.defaults.headers['Authorization'] = `Bearer ${token}`;
    await axios.get("https://secondhandkelompok4.herokuapp.com/api/users/data")
            .then(async (response)=> {
              let result = await response.data;
              setUser(response.data)
            })
  }

  const checkUser = () => {
    try {
      const data = user.user.users;

      if(data.address == "" || null || data.city == "" || null || data.email == "" || null || data.username == "" || null || 
        data.phoneNumber == "" || null){
          alert("Lengkapi dulu yuk profilmu")
          direct("/LengkapiProfil")
          return;
        }
      else{
        direct("/lengkapiproduk")
        return;
      }
    } catch (error) {
      alert("Silahkan login terlebih dahulu")
      direct("/masuk")
    }
  }

  // filter category

  const filterCategory = (itemID) => {
    let temp = [...barangs];
    const filter = temp.filter((item) => item.categoryId == itemID )
    setListBarang(filter)
  }

   const showsAll = () => {
     setListBarang(barangs)
   } 

   const addToken = (e) => {
    e.preventDefault();

    dispatch(setCredentials({
      accessToken: token
    }))
   }

  return (
    <>
      <div className="smHomeSlider">

        {/* NAVBAR */}
        <NavbarHome />
        <Container className="d-block d-sm-none pt-4">
          <div class="row">
            <div class="col">
              <h2>
                <b>Bulan Ramadhan Banyak Diskon!</b>
              </h2>
              <br></br>
              <h6>Diskon Hingga</h6>
              <h4 style={{ color: "red" }}>60 %</h4>
            </div>
            <div class="col">
              <img src={gift} alt="gift"></img>
            </div>
          </div>
        </Container>

        {/* CAROUSEL */}
        <Container className="mt-4 ">
          <Carousel className="d-none d-sm-block">
            <Carousel.Item>
              <img
                className="d-none d-sm-block w-100"
                src={carousel1}
                alt="First slide"
              />
              <Carousel.Caption>
                <p></p>
              </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
              <img
                className="d-none d-sm-block w-100"
                src={carousel1}
                alt="Second slide"
              />
              <Carousel.Caption>
                <p></p>
              </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
              <img
                className="d-none d-sm-block w-100"
                src={carousel1}
                alt="Third slide"
              />
              <Carousel.Caption>
                <p></p>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
        </Container>

        {/* BARIS */}
        <Container>
          <br></br>
        </Container>
      </div>

      {/* TELUSURI KATEGORI */}
      <Container>
        <h5>Telusuri Kategori</h5>

        <Row xs="auto">
          <div style={{ display: "flex" }} className="menuKategori">
            <Button className="btnCategories" onClick={() => showsAll()}>
              <img src={iconSearch} className="iconImg" alt="icon" />
              Semua
            </Button>

            <Button className="btnCategories" onClick={() => filterCategory('1')}>
              <img src={iconSearch} className="iconImg" alt="icon" />
              Elektronik
            </Button>

            <Button className="btnCategories" onClick={() => filterCategory('2')}>
              <img src={iconSearch} className="iconImg" alt="icon" />
              Pakaian
            </Button>

            <Button className="btnCategories" onClick={() => filterCategory('3')}>
              <img src={iconSearch} className="iconImg" alt="icon" />
              Olahraga
            </Button>

            <Button className="btnCategories" onClick={() => filterCategory('4')}>
              <img src={iconSearch} className="iconImg" alt="icon" />
              Hobi
            </Button>

            <Button className="btnCategories" onClick={() => filterCategory('5')}>
              <img src={iconSearch} className="iconImg" alt="icon" />
              Mainan
            </Button>
          </div>
        </Row>
      </Container>

      {/* BARIS */}
      <Container>
        <br></br>
      </Container>

      {/* CARD */}
      <Container>
        <Row xs={2} md={3} lg={6} className="g-4">
          {barang.length == 0 ? (
            <LoadingSkeletonCard cards={6}/>
          ) : (
            listBarang.map((item) => {
              if(item.isAvailable == 1 && item.isTerjual == null || item.isTerjual == "" || item.isTerjual == "BATAL"){
                const link = `https://finalproj-fsw8-4-frontend.vercel.app/your/product/${item.id}`
                const str = item.postedby.fullname;
                const word = str.split(" ");
                const hasil = word[0]

                const words = item.nama_barang.split(" ");
                const result = words[0] + " " + words[1]
              return (
                  <Col key={item.id}>
                  <Card style={{ minHeight: "280px" }} className="mb-4 cardHome">
                    <a href={link}>
                    <Card.Img
                      variant="top"
                      className="p-2 image-fluid productImageHome"
                      src={item.image[0]}
                    />
                    </a>
                    <Card.Body>
                      <Card.Title className="h1Card">
                        <b>{result}</b>
                      </Card.Title>
                      <Card.Subtitle className="h2Card my-1">
                        by {hasil}
                      </Card.Subtitle>
                      <Card.Text className="h3Card mt-3">
                        Rp {item.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
              );
              }
            })
          )}
        </Row>
      </Container>
      <Container>
        <Button className="btnJual" onClick={checkUser}>
          + Jual
        </Button>
         {/* <Button className="btnJual" onClick={addToken}>
          Check Redux
          </Button> */}
      </Container>
    </>
  );
}
export default HomePage;

import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useParams } from "react-router-dom";
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import NavbarPlain from '../components/NavbarPlain';
import { ArrowLeft } from 'react-bootstrap-icons';
import ButtonMediumPurple from '../components/ButtonMediumPurple';
import BtnMdSecondary from '../components/BtnMdSecondary';
import inputPhotoProfile2 from '../assets/img/inputPhotoProfile2.svg';
import { WhatsApp } from "@mui/icons-material";
import axios from "axios";
import LoadingPart from "../components/LoadingPart";

function InfoPenawarPage() {

  let { id } = useParams();

  const [iden, setIden] = useState(null)

  // API for buyer
  const [username, setUsername] = useState([]);
  const [minat, setMinat] = useState([]);
  const [city, setCity] = useState('')
  const token = localStorage.getItem("auth_token");
  const [loading, setLoading] = useState(false);
  

  const fetchUserData = async() => {
    setLoading(true);
    axios.defaults.headers['Authorization'] = `Bearer ${token}`

    await axios.get("https://secondhandkelompok4.herokuapp.com/api/users/data")
    .then(async(response) => {
      let hasil = response.data;
      setUsername(hasil.user.users);
      setLoading(false);
    })
  };

  const fetchMinatData = async() => {
    setLoading(true);
    axios.defaults.headers['Authorization'] = `Bearer ${token}`

    await axios.get(`https://secondhandkelompok4.herokuapp.com/api/negos/find/${id}`)
    .then(async(res)=> {
      let hasil = res.data.nego;
      setMinat(hasil[0]);
      setLoading(false);
    })
  }

  const Reject = async (id) => {
    axios.defaults.headers['Authorization'] = `Bearer ${token}`

    await axios.put(`https://secondhandkelompok4.herokuapp.com/api/negos/update/${id}`, {
      decision: "REJECT"
    })
    .then((res) => {
      alert("Tawaran nego ditolak oleh penjual!");
      window.location.href = '/DaftarJualDiminati';
    })
  }

  const Acc = async (id) => {
    axios.defaults.headers['Authorization'] = `Bearer ${token}`

    await axios.put(`https://secondhandkelompok4.herokuapp.com/api/negos/update/${id}`, {
      decision: "ACCEPT"
    })
    .then((res) => {
      alert("Tawaran nego berhasil diterima, segera lakukan kontak dari pembeli");
      window.location.reload();
    })
  }

  const Tolak = async () => {
    axios.defaults.headers['Authorization'] = `Bearer ${token}`

    await axios.put(`https://secondhandkelompok4.herokuapp.com/api/negos/updateterjualtolak/${id}`, {
      decision: "BATAL"
    })
    .then((res) => {
      alert("Pesanan dibatalkan oleh penjual");
      window.location.reload();
    })
  }

  const Terjual = async () => {
    axios.defaults.headers['Authorization'] = `Bearer ${token}`

    await axios.put(`https://secondhandkelompok4.herokuapp.com/api/negos/updateterjual/${id}`, {
      decision: "TERJUAL"
    })
    .then((res) => {
      alert("Pesanan sukses terjual");
      window.location.reload();
    })
  }

  useEffect(() => {
    fetchUserData();
    fetchMinatData()
  }, []); 

    if (minat.sendid) {
      const linkWA = `https://wa.me/${minat.sendid.phoneNumber}`
      return (
        <>
         
          {/* NAVBAR */}
          <NavbarPlain judulPage="Info Penawar"/>

          {
            loading === true ? (
              <LoadingPart/>
            ) : (
              <>
            {/* KONTEN */}
            <Container fluid className='pt-5'>
  
              <Row>
                <Col md={3}>
                <a className='iconArrowLeft' href='/daftarjualdiminati' style={{ color: "black" }}>
                    <ArrowLeft size={34} className=''/>
                </a>
                </Col>
              </Row>
            
              {/* Card Penawar */}
              <Row>
                <Col lg={12}>
                    <Container>
                      <Card className='cardPenawar'>
                          <Card.Body>
                          <div className="kontenCardPenawar">
                          <Row>
                              <Col>
                                { minat.sendid.image !== 0 && minat.sendid.image!== null ? (
                                  <img src={minat.sendid.image} alt="foto" className='fotoAkunSmallVer'/>
                                    ) : (
                                  <img src={inputPhotoProfile2} alt="none" className='fotoAkunSmallVer'/>
                                  )
                                }
                              </Col>
                              <Col>
                                <div className="txtCardPenawar">
                                <p><b>{minat.sendid.fullname}</b></p>
                                <p>{minat.sendid.city}</p>
                                </div>
                              </Col>
                          </Row>
                          </div>
                          </Card.Body>
                      </Card>
                    </Container>
                  </Col> 
              </Row>
  
              {/* Daftar Produk */}
              <Row>
                <Col lg={12}>
                  <Container>
                    <p className="titleTawar"><b>Daftar produkmu yang ditawar</b></p>
  
                    <Card className="float-right cardBarangDitawar">
                        {
                          minat.length !== 0 ? (
                                <Row>
                                <Col sm={5} className="text-center">
                                  <img className='imgBarangDitawar' src={ minat.itemid.image[0] } alt=""
                                  style={{ 
                                    width: "100%",
                                    height: "10em",
                                    objectFit: "cover",
                                    marginTop: "1.5rem",
                                    borderRadius: "8px"
                                   }}/>
                                </Col>
                                <Col sm={7}>
                                  <div className="kontenCardBarangDitawar">
                                      <p></p>
                                      <h6 className="card-title"><b>{minat.itemid.nama_barang}</b></h6>
                                      <p>Rp {minat.itemid.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</p>
                                      <p><b>Ditawar Rp {minat.tawar.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</b></p>
                                      <p>Ditawar oleh {minat.sendid.fullname}</p>
                                      {
                                        (minat.isAccept == "TBD" || minat.isAccept == "" || minat.isAccept === "REJECT") ? ( 
                                          minat.isTerjual === "BATAL" ? (
                                            <>
                                            <h6>Status : <b style={{color:"red"}}>DITOLAK</b></h6>
                                            </>
                                          ): (
                                            minat.itemid.isTerjual === "TERJUAL" ? (
                                              <>
                                                <h6>Status : <b style={{color:"green"}}>TERJUAL</b></h6>
                                              </>
                                            ) : (
                                              <>
                                              <BtnMdSecondary namaButton="Tolak" onClick={(e) => {
                                              Reject(minat.id)
                                            }}/>
                                            <ButtonMediumPurple namaButton="Terima" onClick={(e) => {
                                              Acc(minat.id)
                                            }}/>
                                            </>
                                            )
                                          )
                                        ) : (
                                          minat.isTerjual === "TERJUAL" || minat.itemid.isTerjual === "TERJUAL" ? (
                                          <>
                                            <h6>Status : <b style={{color:"green"}}>TERJUAL</b></h6>
                                          </>
                                          ) : (
                                            <>
                                              <a href={linkWA} target="_blank">
                                              <Button className="btn btnSecondaryMd mt-4">
                                                  <WhatsApp style={{ marginRight: "0.5rem", marginBottom: "0.2rem"}} />
                                                  Penawar
                                              </Button>
                                              </a>

                                            <Button className="btn btnPurpleMd mt-4" 
                                                type="button" 
                                                data-bs-toggle="modal" 
                                                data-bs-target="#exampleModal">
                                                Status
                                            </Button>
                                              <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div className="modal-dialog">
                                                <div className="modal-content">
                                                  <div className="modal-header">
                                                    <h5 className="modal-title" id="exampleModalLabel">Perbarui status penjualan</h5>
                                                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                  </div>
                                                  <div className="modal-body">
                                                    <h6 style={{color:"#8A8A8A"}}> Jadi dijual atau batal nih?</h6>
                                                    <div style={{ background: "#EEEEEE", boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.15)", borderRadius: "16px" }} classname="productMatchBox">
                                                      <div className="row" style={{ paddingTop:'4%', textAlign:'center'}}>
                                                        <h6><b>Product Match</b></h6>
                                                      </div>
                                                      <div className="row" >
                                                        <div className="col-2 p-3">
                                                          { minat.sendid.image !== 0 && minat.sendid.image!== null ? (
                                                            <img src={minat.sendid.image} alt="foto" className='fotoAkunSmallVer'/>
                                                              ) : (
                                                            <img src={inputPhotoProfile2} alt="none" className='fotoAkunSmallVer'/>
                                                            )
                                                          }
                                                        </div>
                                                        <div className="col-10 p-2 txtModal1" style={{lineHeight: '75%', marginTop:'2%'}}>
                                                          <h6><b>{minat.sendid.fullname}</b></h6>
                                                          <p>{minat.sendid.city}</p>
                                                        </div>
                                                      </div>
                                                      <div className="row">
                                                        <div className="col-2 p-3">
                                                          <img src={ minat.itemid.image[0] } alt="iconseller" className="fotoModal" />
                                                        </div>
                                                        <div className="col-10 p-2 txtModal1" style={{lineHeight: '75%', marginTop:'1%', paddingLeft:'10%'}}>
                                                            <h6 className="card-title"><b>{minat.itemid.nama_barang}</b></h6>
                                                            <p style={{ textDecoration: "line-through"}}>Rp {minat.itemid.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</p>
                                                            <p>Ditawar Rp {minat.tawar.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</p>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <BtnMdSecondary namaButton="BATALKAN" onClick={(e) => {Tolak(minat.id)}}/>
                                                    <ButtonMediumPurple namaButton="TERJUAL" onClick={(e) => {Terjual(minat.id)}}/>
                                                  </div>
                                                </div>
                                              </div>
                                              </div>
                                            </>
                                          )
                                        )
                                      }
                                  </div>
                              </Col>
                              </Row>
                              )
                           : (
                            <p>Produkmu belum ada yang ditawar</p>
                          )
                        }
                    </Card>
  
                  </Container>
                </Col>
              </Row>

              </Container>
              </>
            )
          }
  

        </>
      );
    }
    
  };

  export default InfoPenawarPage;
import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "../Global.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import NavbarNotif from '../components/NavbarNotif';
import BtnSmSecondary from '../components/BtnSmSecondary';
import ArrowLeft from '../assets/img/arrowLeft.svg';
import ArrowLeftPurple from '../assets/img/arrowLeftPurple.svg';
import iconBoxPurple from '../assets/img/iconBoxPurple.svg';
import iconMoney from '../assets/img/iconMoney.svg';
import iconHeart from '../assets/img/iconHeart.svg';
import tambahProduk from '../assets/img/tambahProduk.svg';
import MenuSeller from '../components/MenuSeller';
import illustrasiSeller from '../assets/img/illustrasiSeller.svg';
import inputPhotoProfile2 from '../assets/img/inputPhotoProfile2.svg';
import axios from "axios";
import LoadingPart from "../components/LoadingPart";

function DaftarJualProdukPage() {
  // API for seller
  const [username, setUsername] = useState("");
  const [city, setCity] = useState("");
  const [barang, setBarang] = useState([]);
  const token = localStorage.getItem("auth_token");
  const [loading, setLoading] = useState(false)

  const fetchUserData = async () => {
    axios.defaults.headers["Authorization"] = `Bearer ${token}`;

    await axios
      .get("https://secondhandkelompok4.herokuapp.com/api/users/data")
      .then(async (response) => {
        let hasil = await response.data;
        setUsername(hasil.user.users);
        setCity(hasil.user.users.city);
      });
  };

  const fetchBarangUser = async () => {
    setLoading(true)
    axios.defaults.headers["Authorization"] = `Bearer ${token}`;

    await axios
      .get("https://secondhandkelompok4.herokuapp.com/api/sellers/barangseller")
      .then(async (response) => {
        setBarang(response.data.barang);
      });
    setLoading(false)
  };

  useEffect(() => {
    if (username == "" && token !== "") {
      fetchUserData();
      fetchBarangUser();
      return;
    }
  });

  // page

  if(loading === true){
    return(
      <>
         {/* Page loading */}
         <NavbarNotif/>
        <LoadingPart/>
      </>
    )
  }

  return (
    <>
      {/* NAVBAR */}
      <NavbarNotif />

      {/* KONTEN */}
      <Container fluid className="pt-5">
        {/* Title */}
        <Row>
          <Col md={4}>
            <div className="titleDaftarJual">
              <h4>
                <b>Daftar Jual Saya</b>
              </h4>
            </div>
          </Col>
        </Row>

        {/* Card Penjual */}
        <Row>
          <Col lg={12}>
            <Container>
              <Card className="cardSeller">
                <Card.Body>
                  <div className="kontenCardSeller">
                    <Row>
                      <Col>
                        { username.image !== 0 && username.image !== null ? (
                            <img src={username.image} alt="foto" className='fotoAkunSmallVer'/>
                              ) : (
                            <img src={inputPhotoProfile2} alt="none" className='fotoAkunSmallVer'/>
                          )
                        }
                      </Col>
                      <Col>
                        <div className="txtCardSeller">
                          <p>
                            <b>{username.fullname}</b>
                          </p>
                          <p>{username.city}</p>
                        </div>
                      </Col>
                      <Col className="btnEditSeller mt-3">
                        <BtnSmSecondary
                          namaButton="Edit"
                          type="button "
                          linkHref="/lengkapiProfil"
                        />
                      </Col>
                    </Row>
                  </div>
                </Card.Body>
              </Card>
            </Container>
          </Col>
        </Row>

        {/* Menu Penjual */}
        <Container className="pageDaftarJual mb-4">
          <Row>
            <Col lg={3}>
              <Card className="cardMenuPenjual">
                <div className="kontenCardMenuPenjual">
                  <p>
                    <b>Kategori</b>
                  </p>
                  <p>
                    <img src={iconBoxPurple} alt="icon" />
                    <a
                      href="/DaftarJualProduk"
                      className="txtMenuPenjual"
                      id="menuPressed"
                    >
                      <b> Semua Produk </b>{" "}
                    </a>
                    <img src={ArrowLeftPurple} alt="iconArrow" />
                  </p>
                  <hr />
                  <p>
                    <img src={iconHeart} alt="icon" />
                    <a href="/DaftarJualDiminati" className="txtMenuPenjual">
                      {" "}
                      Diminati{" "}
                    </a>
                    <img src={ArrowLeft} alt="iconArrow" />
                  </p>
                  <hr />
                  <p>
                    <img src={iconMoney} alt="icon" />
                    <a href="/DaftarJualTerjual" className="txtMenuPenjual">
                      {" "}
                      Terjual{" "}
                    </a>
                    <img src={ArrowLeft} alt="iconArrow" />
                  </p>
                </div>
              </Card>

              {/* for smaller device (menu button) */}
              <MenuSeller />
            </Col>

            {/* Produk */}
            <Col lg={9} className="colJual">
              <Row xs={2} md={3} className="g-4 kontenProduk">
                <Col>
                  <a href="/LengkapiProduk">
                    <img
                      src={tambahProduk}
                      alt="icon"
                      className="imgTambahProduk"
                    />
                  </a>
                </Col>
                {barang.length == 0 ? (
                  <div className="belumJual mb-5">
                    <img src={illustrasiSeller} alt="icon" />
                    <p>Kamu belum menjual apa-apa, nih</p>
                  </div>
                ) : (
                  barang.map((item) => {
                    const link = `https://finalproj-fsw8-4-frontend.vercel.app/product/${item.id}`;
                    return (
                      <>
                        <Col>
                          <a
                            href={link}
                            className="text-decoration-none"
                            style={{ color: "black" }}
                          >
                            <Card className="txtCardProduk mb-2" style={{ minHeight: "280px" }}>
                              <Card.Img 
                                  variant="top"
                                  className="p-2 image-fluid productImage"
                                  src={item.image[0]} />
                              <Card.Body className="txtCardProduk">
                                <p className="h1Card">
                                  <b>{item.nama_barang}</b>
                                </p>
                                <p className="h2Card">{item.categoryid.nama}</p>
                                <p className="h1Card">
                                  Rp{" "}
                                  {item.price
                                    .toString()
                                    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                                </p>
                              </Card.Body>
                            </Card>
                          </a>
                        </Col>
                      </>
                    );
                  })
                )}
              </Row>
            </Col>
          </Row>
        </Container>
      </Container>
    </>
  );
}

export default DaftarJualProdukPage;

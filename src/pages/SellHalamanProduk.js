import { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Carousel from "react-bootstrap/Carousel";
import LoadingPart from "../components/LoadingPart";
import iconSeller1 from "../assets/img/iconSeller.png";
import NavbarNotif from "../components/NavbarNotif";
import { getIdTokenResult } from "firebase/auth";
import { Link } from "react-router-dom";
import { Delete, Edit } from "@mui/icons-material";

function SellHalamanProduk() {
  const [post, setPost] = useState({});
  const [nego, setNego] = useState("");
  const [loading, setLoading] = useState(false);
  let { id } = useParams();

  const token = localStorage.getItem("auth_token");

  const fetchData = async (value) => {
    setLoading(true)
    await axios
      .get(
        `https://secondhandkelompok4.herokuapp.com/api/items/findonesell/${value}`
      )
      .then((res) => {
        setPost(res.data.barang[0]);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false)
      });
  };

  const Terbit = async (e) => {
    e.preventDefault();

    axios.defaults.headers["Authorization"] = `Bearer ${token}`;
    await axios
      .get("https://secondhandkelompok4.herokuapp.com/api/users/data")
      .then(async (respond) => {
        let hasil = respond.data;

        setLoading(true);
        await axios
          .put(
            `https://secondhandkelompok4.herokuapp.com/api/items/terbitkan/${id}`
          )
          .then(() => {
            setLoading(false);
            alert("Produk Berhasil Diterbitkan");
            window.location.reload();
          });
      });
  };

  useEffect(() => {
    fetchData(id);
  }, []);
  const link = `https://finalproj-fsw8-4-frontend.vercel.app/editproduct/${post.id}`
  return (
    <>
      {/* NAVBAR */}
      <NavbarNotif />
      {
        loading === true ? (
          <LoadingPart/>
        ) : (
          <>
          {/* PRODUCT */}
          <Container>
            <div class="row mt-4">
              <div class="col-lg-8">
                <Carousel>
                  {
                    post.image ? (
                      post.image.map((item)=>{
                        return(
                          <Carousel.Item>
                            <img
                              className="d-block w-100"
                              src={item}
                              alt="First slide"
                              style={{ 
                                objectFit: "cover"
                              }}
                            />
                          </Carousel.Item>
                        )
                      })
                    ):(
                      <p>No image</p>
                    )
                  }
                </Carousel>
              </div>
              {/* COL KE 2 */}
              <div class="col-lg-4">
                <div class="textShadowBox p-4">
                  <h4>{post.nama_barang}</h4>
                  <h6>Harga :</h6>
                  <h5>
                    Rp{" "}
                    {post.price
                      ? post.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      : post.price}
                  </h5>
                  <div>
                    {
                      post.isAvailable == null || post.isAvailable == 0 ? (
                        <>
                        <div>
                            <Button className="btnPurple w-100 mt-2 mb-2"
                              type="button"
                              onClick={Terbit}>
                              Terbitkan
                            </Button>
                        </div>
                        <div>
                            <a href={link}>
                              <Button className="btnPurpleSc w-100 mt-2 mb-2"
                                  type="button">
                                  <Edit style={{ marginRight: "0.5rem", marginBottom: "0.2rem"}} />
                                  Edit
                              </Button>
                            </a>
                        </div>
                        <div>
                            <Button className="btnRedSc w-100 mt-2 mb-2"
                              type="button">
                              <Delete style={{ marginRight: "0.5rem", marginBottom: "0.2rem"}} />
                              Hapus Barang
                            </Button>
                        </div>
                        </>
                      ) : (
                        post.isTerjual === "TERJUAL" ? (
                          <Button className="btnPurpleSc w-100 mt-2 mb-2" 
                            style={{ 
                              backgroundColor: "#f74343"
                            }}
                          disabled={true}>
                            <b>SUDAH TERJUAL</b>
                          </Button>
                        ) : (
                          <>
                            <a href={link}>
                               <Button className="btnPurpleSc w-100 mt-2 mb-2"
                                type="button">
                                <Edit style={{ marginRight: "0.5rem", marginBottom: "0.2rem"}} />
                                Edit
                              </Button>
                            </a>
                            <div>
                              <Button className="btnRedSc w-100 mt-2 mb-2"
                                type="button">
                                <Delete style={{ marginRight: "0.5rem", marginBottom: "0.2rem"}} />
                                Hapus Barang
                              </Button>
                           </div>
                          </>
                        )
                      )
                    }
                  </div>
                </div>

                <div class="textShadowBox p-4 mt-4">
                  <div className="row justify-content-start">
                    <div className="col-2">
                      <img
                        src={post.postedby ? post.postedby.image : { iconSeller1 }}
                        style={{
                          height: "50px",
                          width: "100%",
                          objectFit: "cover",
                          borderRadius: "8px",
                        }}
                        alt="iconseller"
                      />
                    </div>
                    <div className="col-8">
                      <h5>{post.postedby ? post.postedby.fullname : "Penjual"}</h5>
                      <h6>{post.postedby ? post.postedby.city : "kota"}</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* TEXT DESCRIPSI */}
            <div class="row">
              <div class="col-lg-8">
                <div class="textShadowBox p-4 mt-4 mb-4">
                  <h4>Deskripsi</h4>
                  <div align="justify">{post.description}</div>
                </div>
              </div>
            </div>
          </Container>
              </>
            )
          }
    
    </>
  );
}

export default SellHalamanProduk;
import React, { useState, useEffect } from "react";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import ListOfTodo from "../components/ListOfTodo";
import HomePage from "./HomePage";

const firebaseConfig = {
  apiKey: "AIzaSyAIfClgYmSNXex6jsttIXeSbsYA8X5gA1A",
  authDomain: "radianprojies.firebaseapp.com",
  databaseURL: "https://radianprojies-default-rtdb.firebaseio.com",
  projectId: "radianprojies",
  storageBucket: "radianprojies.appspot.com",
  messagingSenderId: "890314411848",
  appId: "1:890314411848:web:cfdd3bc496d623a7d62d7d",
  measurementId: "G-8C82QTZ56E",
};

firebase.initializeApp(firebaseConfig);

function Testing() {
  const [auth, setAuth] = useState(
    false || window.localStorage.getItem("auth_token") === "true"
  );
  const [token, setToken] = useState('');

  useEffect(() => {
    firebase.auth().onAuthStateChanged((userCredentials) => {
      if (userCredentials) {
        setAuth(true);
        window.localStorage.setItem("auth", "true");
        userCredentials.getIdToken().then((token) => {
          const newToken = localStorage.setItem("auth_token", token);
          const tokenBaru = localStorage.getItem("auth_token");

          setToken(tokenBaru)
          
        });
      }
    });
  }, []);

  const loginWithGoogle = () => {
    firebase
      .auth()
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then((userCredentials) => {
        if (userCredentials) {
          setAuth(true);
          window.localStorage.setItem("auth", "true");
        }
      });
  };
  return (
    <div>
      {auth ? (
        <HomePage tokenize={token} />
      ) : (
        <button onClick={loginWithGoogle}>Login With Google</button>
      )}
    </div>
  );
}

export default Testing;

import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import '../Global.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import NavbarNotif from '../components/NavbarNotif';
import BtnSmSecondary from '../components/BtnSmSecondary';
import ArrowLeft from '../assets/img/arrowLeft.svg';
import ArrowLeftPurple from '../assets/img/arrowLeftPurple.svg';
import iconMoneyPurple from '../assets/img/iconMoneyPurple.svg';
import iconHeart from '../assets/img/iconHeart.svg';
import iconBox from '../assets/img/iconBox.svg';
import illustrasiSeller from '../assets/img/illustrasiSeller.svg';
import MenuSeller from '../components/MenuSeller';
import inputPhotoProfile2 from '../assets/img/inputPhotoProfile2.svg';
import LoadingPart from "../components/LoadingPart";
import axios from "axios";

function  DaftarJualTerjualPage() {

  // API for seller
  const [username, setUsername] = useState('');
  const [city, setCity] = useState('');
  const [terjual, setTerjual] = useState([]);
  const token = localStorage.getItem("auth_token");
  const [loading, setLoading] = useState(false)

  const fetchUserData = async () => {
    axios.defaults.headers['Authorization'] = `Bearer ${token}`

    await axios.get("https://secondhandkelompok4.herokuapp.com/api/users/data")
    .then(async(response) => {
      let hasil = await response.data;
      setUsername(hasil.user.users);
      setCity(hasil.user.users.city);
    })
  };

  const fetchTerjualData = async () => {
    setLoading(true)
    axios.defaults.headers['Authorization'] = `Bearer ${token}`

    await axios.get("https://secondhandkelompok4.herokuapp.com/api/items/findall/sold")
    .then(async(res)=> {
      setTerjual(res.data.items);
    })
    setLoading(false)
  }

  useEffect(() => {
    if(username == '' && token !== ''){
      fetchUserData()
      fetchTerjualData()
    }
  });

  // page

  if(loading === true){
    return(
      <>
         {/* Page loading */}
         <NavbarNotif/>
        <LoadingPart/>
      </>
    )
  }

   if (terjual !== null || terjual.length !== 0) {
    return (
      <>
       
        {/* NAVBAR */}
        <NavbarNotif/>

        {/* KONTEN */}
        <Container fluid className='pt-5'>

            {/* Title */}
            <Row>
              <Col md={4}>
                <div className='titleDaftarJual'>
                    <h4><b>Daftar Jual Saya</b></h4>
                </div>
              </Col>
            </Row>
          
             {/* Card Penjual */}
            <Row>
              <Col lg={12}>
                  <Container>
                    <Card className='cardSeller'>
                        <Card.Body>
                        <div className="kontenCardSeller">
                        <Row>
                            <Col>
                            { username.image !== 0 && username.image !== null ? (
                                <img src={username.image} alt="foto" className='fotoAkunSmallVer'/>
                                  ) : (
                                <img src={inputPhotoProfile2} alt="none" className='fotoAkunSmallVer'/>
                                )
                              }
                            </Col>
                            <Col>
                              <div className="txtCardSeller">
                                <p><b>{ username.fullname }</b></p>
                                <p>{ username.city }</p>
                              </div>
                            </Col>
                            <Col className="btnEditSeller mt-3">
                              <BtnSmSecondary namaButton="Edit" type="button "linkHref="/lengkapiProfil"/>
                            </Col>
                        </Row>
                        </div>
                        </Card.Body>
                    </Card>
                  </Container>
                </Col> 
            </Row>

            {/* Menu Penjual */}
            <Container className='pageDaftarJual'>
            <Row>
       
              <Col lg={3}>
                <Card className='cardMenuPenjual'>
                  <div className='kontenCardMenuPenjual'>
                      <p><b>Kategori</b></p>
                        <p>
                          <img src={iconBox} alt="icon"/>
                          <a href="/DaftarJualProduk" className="txtMenuPenjual">    Semua Produk        </a>        
                          <img src={ArrowLeft} alt="iconArrow"/>
                        </p>
                      <hr/>
                      <p>
                          <img src={iconHeart} alt="icon"/>
                          <a href="/DaftarJualDiminati" className="txtMenuPenjual">  Diminati     </a>        
                          <img src={ArrowLeft} alt="iconArrow"/>
                      </p>
                      <hr/>
                      <p>
                        <img src={iconMoneyPurple} alt="icon"/>
                        <a href="/DaftarJualTerjual" className="txtMenuPenjual" id="menuPressed"><b>  Terjual     </b></a>        
                        <img src={ArrowLeftPurple} alt="iconArrow"/>
                      </p>
                  </div>
                </Card>

                {/* for smaller device (menu button)*/}
                <MenuSeller/>

              </Col>


              {/* Produk */}
              <Col lg={9} className='colJual'>
              <Row xs={2} md={3} className="g-4">
                      {
                        terjual.length > 0 ? (
                          terjual.map((item) => {
                            const link = `https://finalproj-fsw8-4-frontend.vercel.app/InfoPenawar/${item.id}`
                            return(
                              
                              <Col>
                                <a href={link} className="text-decoration-none" style={{color:'black'}}>
                                <Card className="txtCardProduk mb-2" style={{ minHeight: "280px" }}>
                                  <Card.Img 
                                    variant="top"
                                    className="p-2 image-fluid productImage"
                                    src={item.itemid.image[0]} />
                                  <Card.Body className='txtCardProduk'>
                                    <p className="h1Card"><b>{item.itemid.nama_barang}</b></p>
                                    <p className="h1Card">From Rp <b style={{ 
                                      textDecoration: "line-through"
                                    }}>{item.itemid.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</b></p>
                                    <p className="h1Card">To Rp {item.tawar.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</p>
                                    <p className="h1Card">Dibeli oleh <b>{item.sendid.firstName}</b></p>
                                  </Card.Body>
                                  </Card>
                                </a>
                            </Col>
                            
                            )
                          })
                        ) : (
                        <div className='belumMinat'>
                          <img src={illustrasiSeller} alt="icon"/>
                          <p>Belum ada produkmu yang terjual nih, <br/>sabar ya rezeki nggak kemana kok</p>
                        </div>
                        )
                    }
              </Row>
              </Col>
             

      
            </Row>
          </Container>

        </Container>
      </>
    );
   }
  
  };

  export default DaftarJualTerjualPage;
import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import NavbarNotif from "../components/NavbarNotif";
import axios from "axios";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "../style.css";
import LoadingPart from "../components/LoadingPart";

function NotifikasiMobile() {
  const token = localStorage.getItem("auth_token");
  const [notif, setNotif] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchNotif = async () => {
    setLoading(true);
    axios.defaults.headers["Authorization"] = `Bearer ${token}`;

    await axios
      .get("https://secondhandkelompok4.herokuapp.com/api/notifications/read")
      .then(async (respond) => {
        let hasil = await respond.data;
        setNotif(hasil.data);
        setLoading(false);
      });
  };

  useEffect(() => {
    fetchNotif();
  }, []);

  return (
    <>
      {/* NAVBAR */}
      <NavbarNotif />

      {
        loading === true ? (
          <LoadingPart/>
        ) : (
          <>
           {/* PRODUCT */}
            <Container>
              {notif !== null || notif !== undefined ? (
                notif.map((item) => {
                  const link = `https://finalproj-fsw8-4-frontend.vercel.app/InfoPenawar/${item.id}`;
                  const linkbuyer = `https://finalproj-fsw8-4-frontend.vercel.app/your/product/${item.barangId}`
                  const words = item.barangid.nama_barang.split(" ");
                  const resultNamaBarang = words[0] + " " + words[1];
                  const word = item.senderid.fullname.split(" ");
                  const resultNamaPenawar = word[0] + " " + word[1];

                  return (
                    <>
                      <hr style={{ margin: "auto" }} width="100%" />
                      <a href={linkbuyer} className="boxNotifMobile dropdown-item">
                        <Row className="p-4">
                          <Col className="p-1" xs={12} md={3}>
                            <img
                              src={item.barangid.image[0]}
                              alt="iconnotif"
                              style={{
                                width: "15em",
                                height: "10em",
                                objectFit: "contain",
                                borderRadius: "8px",
                              }}
                            ></img>
                          </Col>
                          <Col xs={12} md={4}>
                            <h6 className="h2Card">{item.categoryid.type}</h6>
                            <h6>{resultNamaBarang}</h6>
                            {item.tawar !== null ? (
                              <>
                                <h6>Rp <b style={{textDecoration: "line-through"}}>{item.barangid.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</b></h6>
                                <h6>Rp {item.tawar.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</h6>
                                <h6>Ditawar oleh {resultNamaPenawar}</h6>
                                <h6>
                                  <b>STATUS</b> {item.status}
                                </h6>
                              </>
                            ) : (
                              <>
                                <h6>Rp {item.barangid.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</h6>
                                <h6>Dibuat oleh {resultNamaPenawar}</h6>
                              </>
                            )}
                          </Col>
                          <Col xs={12} md={4}>
                          </Col>
                        </Row>
                      </a>
                    </>
                  );
                })
              ) : (
                <h2 style={{ position: "relative", top: "40%", left: "40%" }}>
                  You currently have no notification yet!
                </h2>
              )}
            </Container>
          </>
        )
      }

    </>
  );
}

export default NotifikasiMobile;
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import { useEffect, useState } from 'react';
import '../Global.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import iconEdit from '../assets/img/iconEdit.svg';
import iconLogOut from '../assets/img/iconLogOut.svg';
import iconSetting from '../assets/img/iconSetting.svg';
import NavbarNotif from '../components/NavbarNotif';
import Card from 'react-bootstrap/Card';
import inputPhotoProfile2 from '../assets/img/inputPhotoProfile2.svg';
import NavbarBottomAkun from '../components/NavbarBottomAkun'
import axios from "axios";
import LoadingPart from "../components/LoadingPart";
import LoadingAkunSaya from '../components/LoadingAkunSaya';
import NavbarHome from "../components/NavbarHome"

function AkunSayaPage() {

  // Personalisasi (get API)

    const [username, setUsername] = useState('')
    const [user, setUser] = useState([])
    const token = localStorage.getItem("auth_token");
    const [loading, setLoading] = useState(false);

    const fetchUserData = async () => {
      setLoading(true)
      axios.defaults.headers['Authorization'] = `Bearer ${token}`

      await axios.get("https://secondhandkelompok4.herokuapp.com/api/users/data")
      .then(async(response) => {
        let hasil = await response.data;
        setUser(hasil.user.users)
        setUsername(hasil.user.users.firstName);
      })
      setLoading(false)
    };

    useEffect(() => {
      if(username == '' && token !== ''){
        fetchUserData()
      }
    }, []);

  // function to log out

    const logOut = () => {
      localStorage.clear();
      window.location.href = '/';
    };

  // page

    if(loading === true) {
      return(
        <LoadingAkunSaya/>
      )
    }
    
    const image = user.image
    return (
      <>
        {/* NAVBAR */}
        <div className="navbarAkunSaya"> <NavbarHome/> </div>
            <>
             {/* KONTEN */}
              <Container fluid className='pt-5'>
                <Row>
                    <Col md={4}>
                      <div className='titleAkunSaya'>
                          <p style={{color:'#4b1979'}}>Akun Saya</p>
                          <h3><b>Halo, {username}!</b></h3>
                      </div>
                    </Col>
                </Row>

                  {/* Form Lengkapi Profil */}
                  <Row>
                    <Col lg={4}></Col>
                    <Col lg={4} md={6}>
                      
                    <div className='kontenAkunSaya'>
                      <div className='mt-md-6' style={{paddingBottom: '22%'}}>
                              <div className="mt-4" controlId="formBasicProfilePhoto">
                                  <div className={'text-center'}>
                                      { image == "" || image == null || image == undefined ? (
                                        <img src={inputPhotoProfile2} alt="none" className='fotoAkunNew'/>
                                        ) : (
                                          <img src={image} alt="foto" className='fotoAkunNew'/>
                                        )
                                      }
                                    </div>
                              </div>
                        </div>

                          <Card className='cardAkunSaya mb-5'>
                              <div className="mb-3">
                                  <a className="akunSayaMenu" href="/lengkapiprofil">
                                      <img src={iconEdit} alt="icon"/>    Ubah Akun
                                  </a>
                                  <hr/>
                              </div>
                              <div className="mb-3">
                                  <a className="akunSayaMenu">
                                      <img src={iconSetting} alt="icon"/>     Pengaturan Akun
                                  </a>
                                  <hr/>
                              </div>
                              <div className="mb-3">
                                  <a className="akunSayaMenu" href="/" onClick={logOut}>
                                      <img src={iconLogOut} alt="icon"/>    Keluar
                                  </a>
                                  <hr className='mb-5'/>
                              </div>
                              </Card>
                            </div>
                      </Col>
                      <Col md={4}></Col>
                  </Row>

                {/* NAVBAR BOTTOM for mobile display */}
                <Row>
                  <div className="bttmNavbarAkun">
                    <NavbarBottomAkun/>
                  </div>
                </Row>
              
                
              </Container>
            </>
       
      </>
    );
  };

  export default AkunSayaPage;
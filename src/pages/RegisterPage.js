import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "../Global.css";
import Form from "react-bootstrap/Form";
import coverSecondHand from "../assets/img/coverSecondHand.png";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SecondHand from "../assets/img/SecondHand.svg";
import ButtonLargePurp from "../components/ButtonLargePurp";
import { ArrowLeft } from "react-bootstrap-icons";
import { useNavigate } from "react-router-dom";
import axios from "axios";

function RegisterPage() {

  // validasi untuk tiap field yang kosong
  const [validated, setValidated] = useState(false);

  // data
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const direct = useNavigate();

  const registerAccount = async (e) => {
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.preventDefault();
      e.stopPropagation();
    }

    e.preventDefault();
    setValidated(true);
    await axios.post(
      `https://secondhandkelompok4.herokuapp.com/api/users/registeruser`,
      {
        username: username,
        email: email,
        password: password,
      }
    );
    alert("Akun berhasil di daftarkan");
    direct("/masuk");
  };

  return (
    <>
      {/* KONTEN */}
      <Container fluid>
        <Row>
          {/* Foto */}
          <Col md={6}>
            <div id="overlay">
              <img src={SecondHand} alt="" id="secondHand" />
              <img
                src={coverSecondHand}
                style={{ height: "100%", width: "40%" }}
                alt=""
                id="imgRegister"
              />
            </div>
          </Col>

          {/* Form Register atau Daftar */}
          <Col md={6} className="mt-lg-5">
            <Container className="signUpText">
              <a className="iconArrowRegis" href="/" style={{ color: "black" }}>
                <ArrowLeft size={34} className="" />
              </a>

              <h3 className="mb-lg-4">
                <b>Daftar</b>
              </h3>
              <Form
                validated={validated}
                onSubmit={registerAccount}
                method=" "
                enctype=" "
              >
                <Form.Group className="mb-3 mt-4" controlId="formBasicName">
                  <Form.Label>Nama</Form.Label>
                  <Form.Control
                    type="text"
                    id="username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    placeholder="Nama"
                    className="formRounded"
                    required
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    id="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder="Contoh: johndee@gmail.com"
                    className="formRounded"
                    required
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    id="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder="Masukan password"
                    className="formRounded"
                    required
                  />
                </Form.Group>
                <ButtonLargePurp namaButton="Daftar" />
                <p className="fw-bold mt-2 pt-4 mb-0 txtRegister">
                  Sudah punya akun?
                  <a href="/masuk" className="btnLinkPurp">
                    {" "}
                    Masuk di sini
                  </a>
                </p>
              </Form>
            </Container>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default RegisterPage;
import React from "react";
import { useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "../Global.css";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ButtonLargePurp from '../components/ButtonLargePurp';
import NavbarPlain from "../components/NavbarPlain";
import { ArrowLeft } from "react-bootstrap-icons";
import inputPhotoProduk from "../assets/img/inputPhotoProduk.svg";
import axios from "axios";
import ReactLoading from 'react-loading'
import { useNavigate, useParams } from "react-router-dom";

function InfoProdukPage() {
  let { id } = useParams();

  const [nama_barang, setNamaBarang] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [categoryId, setCategoryID] = useState("");
  const [file, setFile] = useState([]);
  const [loading, setLoading] = useState(false)

  const token = localStorage.getItem("auth_token")

  const fetchData = async() => {
    axios.defaults.headers['Authorization'] = `Bearer ${token}`

    await axios.get(`https://secondhandkelompok4.herokuapp.com/api/items/findonesell/${id}`)
    .then(async(res) => {
      let hasil = res.data.barang[0];
      setNamaBarang(hasil.nama_barang);
      setPrice(hasil.price);
      setDescription(hasil.description);
      setCategoryID(hasil.categoryId);
      setFile(hasil.image)
    })
  }

  const saveFile = (e) => {
    setFile(e.target.files)
  }
  
  const submitForm = async(e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("nama_barang", nama_barang);
    formData.append("price", price);
    formData.append("category", categoryId);
    formData.append("description", description)
    Object.values(file).forEach(file=> {
      formData.append("picture", file)
    })

    try{
      setLoading(true);
      axios.defaults.headers['Authorization'] = `Bearer ${token}`
      await axios.put(`https://secondhandkelompok4.herokuapp.com/api/items/update/${id}`,
      formData)
      setLoading(false)
      alert("barang sudah berhasil terupdate")
    }catch(error){
      console.log(error);
    }
  }

  const img = { display: 'block', width: 'auto', height: '100%' };
  const [files, setFiles] = useState([]);
  
  const {getRootProps, getInputProps} = useDropzone({
    maxFiles:'3',
    accept: {
      'image/*': []
    },
    onDrop: acceptedFiles => {
      setFiles(acceptedFiles.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      })));
    }
  },
  console.log(files));
  
  const thumbs = files.map(file => (
    <div className='thumb' key={file.name}>
      <div className='thumbInner'>
        <img src={file.preview} style={img} alt="productImgNew"
          onLoad={() => { URL.revokeObjectURL(file.preview) }}
        />
      </div>
    </div>
  ));

  useEffect(() => {
    fetchData();
    return () => files.forEach(file => URL.revokeObjectURL(file.preview));
  }, []);

  const hiddenFileInput = React.useRef(null);

  const handleClick = e => {
  hiddenFileInput.current.click()
  }

  const handleChange = e => {
  setFile(e.target.files[0])
  }

  // page
    return (
      <>
        {/* NAVBAR */}
        <NavbarPlain judulPage="Lengkapi Info Produk" />
  
        {/* KONTEN */}
        <Container fluid className="pt-5">
          <Row>
            <Col md={3}>
              <a
                className="iconArrowLeft"
                href="/DaftarJualProduk"
                style={{ color: "black" }}
              >
                <ArrowLeft size={34} className="" href="/" />
              </a>
            </Col>
          </Row>
  
          {/* Form Info Produk */}
          <Row>
            <Col lg={4}></Col>
            <Col lg={4} md={6}>
              <div className="formInfoProduk">
  
                {
                  loading == false ? (
                    <Form
                      className="mt-md-6"
                      style={{ paddingBottom: "22%" }}
                      action="/item/create"
                      method="post"
                      onSubmit={submitForm}
                      encType="multipart/form-data"
                    >
                      <p style={{textAlign:'right', color:'#4b1979'}} className="mt-4"><b>* Wajib diisi</b></p>
                      <Form.Group className="mb-3 mt-4" controlId="formBasicProd">
                        <Form.Label>Nama Produk <a style={{color:'#4b1979'}}>*</a></Form.Label>
                        <Form.Control
                          type="text"
                          id="nama_barang"
                          placeholder="Nama Produk"
                          className="formRounded"
                          value={nama_barang}
                          onChange={(e) => setNamaBarang(e.target.value)}
                          required
                        />
                      </Form.Group>
  
                      <Form.Group className="mb-3" controlId="formBasicPrice">
                        <Form.Label>Harga Produk <a style={{color:'#4b1979'}}>*</a></Form.Label>
                        <Form.Control
                          type="text"
                          id="price"
                          placeholder="Rp 0.000"
                          className="formRounded"
                          value={price}
                          onChange={(e) => setPrice(e.target.value)}
                          required
                        />
                      </Form.Group>
  
                      <Form.Group className="mb-3" controlId="formBasicCategory">
                        <Form.Label>Kategori <a style={{color:'#4b1979'}}>*</a></Form.Label>
                        <Form.Select
                          type="text"
                          id="category"
                          placeholder="Pilih Kategori"
                          className="formRounded"
                          value={categoryId}
                          onChange={(e) => setCategoryID(e.target.value)}
                          required
                        >
                          <option value="1">Elektronik</option>
                          <option value="2">Pakaian</option>
                          <option value="3">Alat Olahraga</option>
                          <option value="4">Hobi</option>
                          <option value="5">Mainan</option>
                          <option value="" selected>
                            Semua Kategori
                          </option>
                        </Form.Select>
                      </Form.Group>
  
                      <Form.Group className="mb-3" controlId="formBasicDesc">
                        <Form.Label>Deskripsi <a style={{color:'#4b1979'}}>*</a></Form.Label>
                        <Form.Control
                          as="textarea" 
                          rows={8}
                          id="description"
                          placeholder="Tulis deskripsi produk"
                          className="formRounded inputLarge"
                          value={description}
                          onChange={(e) => setDescription(e.target.value)}
                          required
                        />
                      </Form.Group>
  
                      <Form.Group className="mb-3" controlId="formBasicProductPhoto">
                        <Form.Label>
                          Foto Produk <a style={{color:'#4b1979'}}>*</a>
                          <br />
                        </Form.Label>
                                <div>
                                <input type="file" id='file' name='picture' onChange={saveFile} style={{
                                  width: "40em",
                                  height: "3em",
                                  marginTop: "2em",
                                  position: "absolute",
                                  display: "none"
                                }}
                                ref={hiddenFileInput}
                                multiple/>
                                <button type="button" onClick={handleClick} style={{ 
                                  border: "none",
                                  color: "white",
                                  backgroundColor: "white"
                                }}>
                                <img src={inputPhotoProduk} alt="foto" style={{position:'relative'}}/>
                                </button>
                                    <aside className='thumbsContainer' style={{position:'relative', left:'1%'}}>
                                      {thumbs}
                                    </aside>
                                </div>
                      </Form.Group>
  
                      <div>
                        <ButtonLargePurp namaButton="Kirim" type="submit" />
                      </div>
                    </Form>
                    ) : (
                      <>
                      <div className="text-center loadingBar">
                        <ReactLoading type="bars" color="#7126b5"
                          height={120} width={70} />
                      </div>
                      <h5 className='text-center' style={{color:"#a06ece"}}><b>Loading...</b></h5>
                      </>
                    )
                  }
              </div>
            </Col>
            <Col md={4}></Col>
          </Row>
        </Container>
      </>
    );
  }

export default InfoProdukPage;

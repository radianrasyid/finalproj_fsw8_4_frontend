import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "../Global.css";
import Form from "react-bootstrap/Form";
import coverSecondHand from "../assets/img/coverSecondHand.png";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SecondHand from "../assets/img/SecondHand.svg";
import ButtonLargePurp from "../components/ButtonLargePurp";
import { ArrowLeft } from "react-bootstrap-icons";
import axios from "axios";
import { useNavigate } from "react-router-dom";

function LupaPasswordPage() {
  // 
  const [email, setEmail] = useState("");
  const [Msg, setMsg] = useState("");
  const direct = useNavigate();
  const [validate, setValidate] = useState(false);

  const sendEmail = async (e) => {
          const form = e.currentTarget;
          if (form.checkValidity() === false) {
            e.preventDefault();
          }
          try {
            e.preventDefault();
            setValidate(true);
            let data = await axios
              .post("https://secondhandkelompok4.herokuapp.com/sendemail", {
                email: email,
              })
              .then((respond) => {
                alert("Email telah masuk");
              });
          } catch (error) {
            if (error.response) {
              setMsg(error.response.data);
            }
          }
  }

  return (
    <>
      {/* KONTEN */}
      <Container fluid>
        <Row>
          {/* Foto */}
          <Col md={6} lg={6}>
            <div id="overlay">
              <img
                src={coverSecondHand}
                style={{ height: "100%", width: "40%" }}
                alt=""
                id="imgRegister"
              />
              <img src={SecondHand} alt="" id="secondHand" />
            </div>
          </Col>

          {/* Form Login atau Masuk */}
          <Col md={6} lg={6} className="mt-lg-5">
            <Container className="loginText">
              <a className="iconArrowRegis" href="/" style={{ color: "black" }}>
                <ArrowLeft size={34} className="" />
              </a>

              <h3 className="mb-lg-1">
                <b>Lupa Password?</b>
              </h3>
              <p className="mb-sm-5">Masukkan email Anda untuk mengatur ulang password</p>
              <Form validated={validate}  onSubmit={sendEmail} >
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    type="email"
                    placeholder="Enter email"
                    className="formRounded"
                  />
                </Form.Group>
                <ButtonLargePurp namaButton="Kirim" />
                <p className="fw-bold mt-2 pt-4 mb-0 txtRegister">
                  Ingat password?
                  <a href="/masuk" className="btnLinkPurp">
                    {" "}
                    Masuk di sini
                  </a>
                </p>
              </Form>
            </Container>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default LupaPasswordPage;

// <buttonLargePurple namaButton="Test"/>

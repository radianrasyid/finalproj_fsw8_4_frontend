import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "../Global.css";
import Form from "react-bootstrap/Form";
import coverSecondHand from "../assets/img/coverSecondHand.png";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SecondHand from "../assets/img/SecondHand.svg";
import ButtonLargePurp from "../components/ButtonLargePurp";
import { auth, loginWithEmailAndPassword, signInWithGoogle } from "../firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import { ArrowLeft } from "react-bootstrap-icons";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import logoGoogle from "../assets/img/logoGoogle.png";
import { useDispatch } from "react-redux";
import { setCredentials } from "../redux/authSlice"

function LoginPage() {

  const dispatch = useDispatch();
  const [user, loading, error] = useAuthState(auth);
    const direct = useNavigate();
    useEffect(() => {
      if(loading){
          return;
      }
      if(user){
          direct('/')
      }
  },[user, loading]);

  // Login
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [Msg, setMsg] = useState("");
  const [validate, setValidate] = useState(false);
  const [token, setToken] = useState();

  const LoginAccount = async (e) => {
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.preventDefault();
    }
    try {
      e.preventDefault();
      setValidate(true);
      let data = await axios
        .post("https://secondhandkelompok4.herokuapp.com/api/users/login", {
          email: email,
          password: password,
        })
        .then((respond) => {
          if (respond.data.token) {
            localStorage.setItem("auth_token", respond.data.token);
            let token = localStorage.getItem("auth_token")
            setToken(token);
            direct("/");
          } else {
            alert("invalid");
          }
        });
        let token = localStorage.getItem("auth_token");
        dispatch(setCredentials({
          accessToken: token
        }))
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data);
      }
    }
  };

  return (
    <>
      {/* KONTEN */}
      <Container fluid>
        <Row>
          {/* Foto */}
          <Col md={6} lg={6}>
            <div id="overlay">
              <img
                src={coverSecondHand}
                style={{ height: "100%", width: "40%" }}
                alt=""
                id="imgRegister"
              />
              <img src={SecondHand} alt="" id="secondHand" />
            </div>
          </Col>

          {/* Form Login atau Masuk */}
          <Col md={6} lg={6} className="mt-lg-5">
            <Container className="loginText">
              <a className="iconArrowRegis" href="/" style={{ color: "black" }}>
                <ArrowLeft size={34} className="" />
              </a>

              <h3 className="mb-lg-4">
                <b>Masuk</b>
              </h3>
              <Form validated={validate} onSubmit={LoginAccount}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    type="email"
                    placeholder="Enter email"
                    className="formRounded"
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    required
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    type="password"
                    placeholder="Password"
                    className="formRounded"
                  />
                </Form.Group>
                <a href="/lupapassword" className="btnLinkPurp">
                  {" "}
                  Lupa Password?
                </a>
                <ButtonLargePurp namaButton="Masuk" linkHref="/" />
                <button
                  className="login__btn login__google mt-4 mb-2"
                  onClick={signInWithGoogle}
                >
                <img src={logoGoogle} className="logoGoogle" alt="ggl"/>
                  Masuk dengan Google
                </button>
                <p className="fw-bold mt-2 pt-4 mb-0 txtRegister">
                  Belum punya akun?
                  <a href="/Daftar" className="btnLinkPurp">
                    {" "}
                    Daftar di sini
                  </a>
                </p>
              </Form>
            </Container>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default LoginPage;

// <buttonLargePurple namaButton="Test"/>

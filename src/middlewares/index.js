const admin = require("../config/firebase-config");

class Middleware {
  async decodeToken(req, res, next) {
    const token = req.headers.authorization.split("Bearer ")[1];

    try {
      const decodeValue = admin.auth().verifyIdToken(token);
      if (decodeValue) {
        next();
      }

      return res.json({
        message: "Unauthorized Access",
      });
    } catch (error) {
      console.log(error);
      res.json({message: "Internal error"})
    }
  }
}

module.exports = new Middleware();

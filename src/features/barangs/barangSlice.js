import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    barangs: [],
    status: 'idle',
    error: null
}

const barangSlice = createSlice({
    name: "barangs",
    initialState,
    reducers: {

    }
})

export const selectAllBarang = (state) => state.barangs.barangs;

export default barangSlice.reducer;
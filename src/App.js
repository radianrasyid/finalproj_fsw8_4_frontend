import React from "react";
import { Routes, Route } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import InfoProfilPage from "./pages/InfoProfilPage";
import InfoProdukPage from "./pages/InfoProdukPage";
import InfoProdukPageById from "./pages/InfoProdukPageById";
import InfoPenawarPageById from './pages/InfoPenawarPageById'
import DaftarJualProdukPage from "./pages/DaftarJualProdukPage";
import DaftarJualDiminatiPage from "./pages/DaftarJualDiminatiPage";
import DaftarJualTerjualPage from "./pages/DaftarJualTerjualPage";
import HomePage from "./pages/HomePage";
import SellHalamanProduk from "./pages/SellHalamanProduk";
import UserHalamanProduk from "./pages/UserHalamanProduk";
import NotifikasiMobile from "./pages/NotifikasiMobile";
import AkunSayaPage from "./pages/AkunSayaPage";
import LupaPasswordPage from "./pages/LupaPasswordPage";
import GantiPasswordPage from "./pages/GantiPasswordPage";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/your/product/:id" element={<UserHalamanProduk />} />
        <Route exact path="/Masuk" element={<LoginPage />}></Route>
        <Route exact path="/Daftar" element={<RegisterPage />}></Route>
        <Route exact path="/LupaPassword" element={<LupaPasswordPage />}></Route>
        <Route exact path="/GantiPassword/:token" element={<GantiPasswordPage />}></Route>

        <Route element={<PrivateRoute />}>
            <Route exact path="/LengkapiProfil" element={<InfoProfilPage />}></Route>
            <Route exact path="/LengkapiProduk" element={<InfoProdukPage />}></Route>
            <Route path="/product/:id" element={<SellHalamanProduk />} />
            <Route path="/editproduct/:id" element={<InfoProdukPageById/>} />
            <Route exact path="/InfoPenawar/:id" element={<InfoPenawarPageById />}></Route>
            <Route exact path="/DaftarJualProduk" element={<DaftarJualProdukPage />}></Route>
            <Route exact path="/DaftarJualDiminati" element={<DaftarJualDiminatiPage />}></Route>
            <Route exact path="/DaftarJualTerjual" element={<DaftarJualTerjualPage />}></Route>
            <Route exact path="/AkunSaya" element={<AkunSayaPage />}></Route>
            <Route path="/NotifikasiMobile" element={<NotifikasiMobile />} />
        </Route>

      </Routes>
    </>
  );
}

export default App;
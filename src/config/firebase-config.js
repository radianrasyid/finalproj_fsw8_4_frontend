// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAIfClgYmSNXex6jsttIXeSbsYA8X5gA1A",
  authDomain: "radianprojies.firebaseapp.com",
  databaseURL: "https://radianprojies-default-rtdb.firebaseio.com",
  projectId: "radianprojies",
  storageBucket: "radianprojies.appspot.com",
  messagingSenderId: "890314411848",
  appId: "1:890314411848:web:cfdd3bc496d623a7d62d7d",
  measurementId: "G-8C82QTZ56E"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);


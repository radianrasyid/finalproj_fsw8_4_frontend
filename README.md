# SecondHand - Front End | Group 4 - FSW 8 - Binar Academy SI Batch 2

SecondHand project is a final task to pass from Fullstack Web Developer bootcamp in Binar Academy. This repository alone is just containing frontend code, if you want to check out the backend, you can click [this](https://gitlab.com/radianrasyid/finalproj_fsw8_4_backend).

## This final project was built by Group 4 - FSW 8 :
- [Tazkia Athariza Dhivara](https://gitlab.com/tazkia.athariza.dhivara-2018)
- [Djaya Dhinata](https://gitlab.com/djayadhinata)
- [Muhammaad Radian Rasyid](https://gitlab.com/radianrasyid)
- [Muhammad Daffa Shidqi](https://gitlab.com/daffashid)
- [Lalu Ocky Saktiya Luhung](https://gitlab.com/laluocky450)

## About this website
SecondHand is an e-commerce web for buying and selling various types of secondhand goods. User can sell and buy throught the same account, then negotiations and transactions occur outside the platform (it could happen on whatsapp).

## Features
 * Register
 * Login 
 * Login With Google
 * Forgot password
 * Logout
 * Available product to purchase
 * Filtered product based on category
 * Search bar on navigation bar
 * Update account information
 * Add new product to sell, preview product, and edit product
 * Negotiation price feature
 * Seller and buyer whatsapp redirect
 * Decline or accept bid
 * User notification 
 * User transaction history 
 * etc

## This program is already Deployed online
You can access this website freely on this link :

```bash
  https://finalproj-fsw8-4-frontend.vercel.app/
```

## Step by step to run this code on your local environment
If you want to run the code from local environtment, you can follow these steps : 
 * Clone this project to your local
 * Run __npm install__ on your terminal, if you haven't installed NodeJs, please install it.
 * Change every redirect link to your localhost link, usually it was "http://localhost:3000"
 * Run __npm start__ on your terminal
 * Wait until the program to opened on your preferred browser tab.

## Tech
Here's the tech that we use to build this project :

| Package            | Version      | 
| ------------------ | ------------ | 
| mui/icons-material | ^5.8.3       | 
| mui/material       | ^5.8.3       | 
| reduxjs/toolkit    | ^1.8.3       | 
| axios              | ^0.27.2"     |
| react-bootstrap    | ^2.4.0       | 
| react-bootstrap-icons | ^1.8.4    | 
| firebase          | ^9.8.4        | 
| jquery            | ^3.6.0        | 
| popper.js         | ^1.16.1       |
| react-dom         | ^18.1.0       | 
|react-loading      | ^2.0.3        | 
| react-firebase-hooks  | ^5.0.3    | 
| react-icons       | ^4.4.0        | 
| [etc (check on this link)](https://gitlab.com/radianrasyid/finalproj_fsw8_4_frontend/-/blob/master/package.json)         |     | 